app.controller('ChangePasswordCtrl',
    function ($scope, $http, $window, $filter) {
        $scope.form_pass = {
            password_actual: '',
            password: '',
            confirmpassword: ''
        };
        $scope.show_pass_actual = false;
        $scope.show_pass = false;
        $scope.show_confirmpass = false;
        $scope.disabled_button = false;

        $scope.submit = function () {
            $scope.disabled_button = true;
            var data = {};
            data.password_actual = hex_sha1(angular.copy($scope.form_pass.password_actual));
            data.password = hex_sha1(angular.copy($scope.form_pass.password));
            data.confirmpassword = hex_sha1(angular.copy($scope.form_pass.confirmpassword));
            $http({
                method: "POST",
                url: base_url + "login/change_pass",
                data: data
            }).then(function (response) {
                notify(response.data.mensaje, 'success');
                setTimeout(function () {
                    $window.location = base_url + 'main';
                }, 1500);
            }).catch(function (err) {
                if (err.status == 500) {
                    notify(err.data.mensaje, 'error');
                    $scope.disabled_button = false;
                } else if (err.status == 400) {
                    notify(err.data.mensaje, 'error');
                    $scope.disabled_button = false;
                }
                else if (err.status == 403) {
                    notify(err.data.mensaje, 'error');
                    $scope.disabled_button = false;
                    setTimeout(function () {
                        $window.location = base_url + 'login/close';
                    }, 1500);
                }
            });
        }
    });


angular.module('app').directive('confirmPassword', function (defaultErrorMessageResolver) {
    defaultErrorMessageResolver.getErrorMessages().then(function (errorMessages) {
        errorMessages['confirmPassword'] = 'Las contrase\u00F1as no coinciden';
    });
    return {
        restrict: 'A',
        require: 'ngModel',
        scope: {
            confirmPassword: '=confirmPassword'
        },
        link: function (scope, element, attributes, ngModel) {
            ngModel.$validators.confirmPassword = function (modelValue) {
                return modelValue === scope.confirmPassword;
            };
            scope.$watch('confirmPassword', function () {
                ngModel.$validate();
            });
        }
    };
});