app.controller('AspirantesCtrl',
    function ($scope, $http, $window) {
        $scope.show_aspirantes = false;
        $scope.show_modal_nacionalidad = false;
        $scope.convocatorias = [];
        $scope.form = { convocatoria_id: '', curso_id: '', nacionalidad: '' };
        $scope.cursos = [];
        $scope.aspirantes = [];
        $scope.mensaje = '';
        $scope.search = '';
        $scope.select = '';
        get_cursos();

        function get_cursos() {
            $http({
                method: "GET",
                url: api_url + "cursos/get_all_cursos_by_administrador_id"
            }).then(function (response) {
                $scope.cursos = response.data.cursos;
                if ($scope.cursos.length == 1) {
                    $scope.form.curso_id = $scope.cursos[0].id;
                    $scope.select_convocatorias();
                }
            }).catch(function (err) {
                if (err.status == 500) {
                    notify(err.data.mensaje, 'error');
                } else if (err.status == 400) {
                    notify(err.data.mensaje, 'error');
                }
                else if (err.status == 403) {
                    notify(err.data.mensaje, 'error');
                    setTimeout(function () {
                        $window.location = base_url + 'login/close';
                    }, 1500);
                }
            });
        }

        $scope.submit = function () {
            var data_send = angular.copy($scope.form);
            $http({
                method: "POST",
                url: api_url + "registros/get_registros",
                data: data_send
            }).then(function (response) {
                $scope.search = '';
                $scope.select = '';
                $scope.aspirantes = response.data.aspirantes;
                if ($scope.aspirantes.length != 0) {
                    $scope.show_modal_nacionalidad = data_send.nacionalidad;
                    $scope.show_aspirantes = true;
                } else {
                    $scope.show_aspirantes = false;
                    $scope.mensaje = 'No hay resultados';
                }

            }).catch(function (err) {
                if (err.status == 500) {
                    notify(err.data.mensaje, 'error');
                } else if (err.status == 400) {
                    notify(err.data.mensaje, 'error');
                }
                else if (err.status == 403) {
                    notify(err.data.mensaje, 'error');
                    setTimeout(function () {
                        $window.location = base_url + 'login/close';
                    }, 1500);
                }
            });
        }

        $scope.get_datos_aspirante = function (registro_id) {
            $http({
                method: "POST",
                url: api_url + "registros/get_datos_aspirante_by_registro_id",
                data: { registro_id: registro_id }
            }).then(function (response) {
                $scope.datos_aspirante = response.data.data;
            }).catch(function (err) {
                if (err.status == 500) {
                    notify(err.data.mensaje, 'error');
                } else if (err.status == 400) {
                    notify(err.data.mensaje, 'error');
                }
                else if (err.status == 403) {
                    notify(err.data.mensaje, 'error');
                    setTimeout(function () {
                        $window.location = base_url + 'login/close';
                    }, 1500);
                }
            });
        }

        $scope.select_convocatorias = function () {

            var tipo_convocatoria_id = '';
            for (var index = 0; index < $scope.cursos.length; index++) {
                if ($scope.cursos[index].id == $scope.form.curso_id) {
                    tipo_convocatoria_id = $scope.cursos[index].tipo_convocatoria_id;
                }
            }
            if (tipo_convocatoria_id != '') {
                $http({
                    method: "POST",
                    url: api_url + "convocatorias/get_convocatorias_by_tipo_convocatoria_id",
                    data: { tipo_convocatoria_id: tipo_convocatoria_id }
                }).then(function (response) {
                    $scope.convocatorias = response.data.convocatorias;
                    if ($scope.convocatorias.length == 1) {
                        $scope.form.convocatoria_id = $scope.convocatorias[0].id;
                    }
                }).catch(function (err) {
                    if (err.status == 500) {
                        notify(err.data.mensaje, 'error');
                    } else if (err.status == 400) {
                        notify(err.data.mensaje, 'error');
                    }
                    else if (err.status == 403) {
                        notify(err.data.mensaje, 'error');
                        setTimeout(function () {
                            $window.location = base_url + 'login/close';
                        }, 1500);
                    }
                });
            }
        }

        $scope.aceptar_aspirante = function (registro_id) {
            $http({
                method: "POST",
                url: api_url + "registros/acept_aspirante_by_registro_id",
                data: { registro_id: registro_id }
            }).then(function (response) {
                notify(response.data.mensaje, 'success');
                $(".modal").modal("hide");
                $scope.submit();
            }).catch(function (err) {
                if (err.status == 500) {
                    notify(err.data.mensaje, 'error');
                } else if (err.status == 400) {
                    notify(err.data.mensaje, 'error');
                }
                else if (err.status == 403) {
                    notify(err.data.mensaje, 'error');
                    setTimeout(function () {
                        $window.location = base_url + 'login/close';
                    }, 1500);
                }
            });

        }
        $scope.rechazar_aspirante = function (registro_id) {
            $http({
                method: "POST",
                url: api_url + "registros/rechazar_aspirante_by_registro_id",
                data: { registro_id: registro_id }
            }).then(function (response) {
                notify(response.data.mensaje, 'success');
                $(".modal").modal("hide");
                $scope.submit();
            }).catch(function (err) {
                if (err.status == 500) {
                    notify(err.data.mensaje, 'error');
                } else if (err.status == 400) {
                    notify(err.data.mensaje, 'error');
                }
                else if (err.status == 403) {
                    notify(err.data.mensaje, 'error');
                    setTimeout(function () {
                        $window.location = base_url + 'login/close';
                    }, 1500);
                }
            });

        }

        $scope.view_document = function (url) {
            $window.open(url, "C-Sharpcorner", "toolbar=yes,scrollbars=yes,resizable=yes,top=100,left=500,width=600,height=600");
        }
    }
);
