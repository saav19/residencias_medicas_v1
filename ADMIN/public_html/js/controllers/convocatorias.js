app.controller('ConvocatoriasCtrl',
    function ($scope, $http, $window) {
        $scope.show_table_convocatorias = false;
        $scope.convocatorias = [];
        $scope.tipos_convocatorias = [];
        $scope.form_convocatoria = { nombre_convocatoria: '', tipo_convocatoria_id: '', fecha_inicio: '', fecha_termino: '' };
        get_convocatorias();
        get_tipos_convocatorias();
        function get_convocatorias() {
            $http({
                method: "GET",
                url: api_url + "convocatorias/get_all_convocatorias"
            }).then(function (response) {
                $scope.convocatorias = response.data.convocatorias;
                $scope.show_table_convocatorias = ($scope.convocatorias.length != 0) ? true : false;
            }).catch(function (err) {
                if (err.status == 500) {
                    notify(err.data.mensaje, 'error');
                } else if (err.status == 400) {
                    notify(err.data.mensaje, 'error');
                }
                else if (err.status == 403) {
                    notify(err.data.mensaje, 'error');
                    setTimeout(function () {
                        $window.location = base_url + 'login/close';
                    }, 1500);
                }
            });
        }

        function get_tipos_convocatorias() {
            $http({
                method: "GET",
                url: api_url + "tipos_convocatorias/get_all_tipos_convocatorias"
            }).then(function (response) {
                $scope.tipos_convocatorias = response.data.tipos_convocatorias;
            }).catch(function (err) {
                if (err.status == 500) {
                    notify(err.data.mensaje, 'error');
                } else if (err.status == 400) {
                    notify(err.data.mensaje, 'error');
                }
                else if (err.status == 403) {
                    notify(err.data.mensaje, 'error');
                    setTimeout(function () {
                        $window.location = base_url + 'login/close';
                    }, 1500);
                }
            });
        }

        $scope.add_convocatoria = function () {
            var data = angular.copy($scope.form_convocatoria);
            $http({
                method: "POST",
                url: api_url + "convocatorias/add_convocatoria",
                data: data
            }).then(function (response) {
                notify(response.data.mensaje, 'success');
                $(".modal").modal("hide");
                get_convocatorias();
            }).catch(function (err) {
                if (err.status == 500) {
                    notify(err.data.mensaje, 'error');
                } else if (err.status == 400) {
                    notify(err.data.mensaje, 'error');
                }
                else if (err.status == 403) {
                    notify(err.data.mensaje, 'error');
                    setTimeout(function () {
                        $window.location = base_url + 'login/close';
                    }, 1500);
                }
            });
        }


        $scope.edit_convocatoria = function (convocatoria_id) {
            var data = {
                nombre_convocatoria: angular.copy($scope.form_convocatoria.nombre_convocatoria),
                tipo_convocatoria_id: angular.copy($scope.form_convocatoria.tipo_convocatoria_id),
                fecha_inicio: angular.copy($scope.form_convocatoria.fecha_inicio),
                fecha_termino: angular.copy($scope.form_convocatoria.fecha_termino),
            };

            $http({
                method: "POST",
                url: api_url + "convocatorias/edit_convocatoria",
                params: { id: convocatoria_id },
                data: data
            }).then(function (response) {
                notify(response.data.mensaje, 'success');
                $(".modal").modal("hide");
                get_convocatorias();
            }).catch(function (err) {
                if (err.status == 500) {
                    notify(err.data.mensaje, 'error');
                } else if (err.status == 400) {
                    notify(err.data.mensaje, 'error');
                }
                else if (err.status == 403) {
                    notify(err.data.mensaje, 'error');
                    setTimeout(function () {
                        $window.location = base_url + 'login/close';
                    }, 1500);
                }
            });
        }

        $scope.reset_form_convovatoria = function () {
            $scope.form_convocatoria = { nombre_convocatoria: '', tipo_convocatoria: '', fecha_inicio: '', fecha_termino: '' };
            document.getElementById('form-convocatoria').reset();
        }

        $scope.get_convocatoria = function (convocatoria_id) {
            $http({
                method: "GET",
                url: api_url + "convocatorias/get_convocatoria_by_id",
                params: { id: convocatoria_id }
            }).then(function (response) {
                $scope.form_convocatoria = response.data.convocatoria;
                $scope.form_convocatoria.fecha_inicio = new Date($scope.form_convocatoria.fecha_inicio);
                $scope.form_convocatoria.fecha_termino = new Date($scope.form_convocatoria.fecha_termino);
            }).catch(function (err) {
                if (err.status == 500) {
                    notify(err.data.mensaje, 'error');
                } else if (err.status == 400) {
                    notify(err.data.mensaje, 'error');
                }
                else if (err.status == 403) {
                    notify(err.data.mensaje, 'error');
                    setTimeout(function () {
                        $window.location = base_url + 'login/close';
                    }, 1500);
                }
            });
        }

        $scope.finish_convocatoria = function (convocatoria_id) {
            $http({
                method: "POST",
                url: api_url + "convocatorias/finish_convocatoria_by_id",
                params: { id: convocatoria_id }
            }).then(function (response) {
                notify(response.data.mensaje, 'success');
                get_convocatorias();
            }).catch(function (err) {
                if (err.status == 500) {
                    notify(err.data.mensaje, 'error');
                } else if (err.status == 400) {
                    notify(err.data.mensaje, 'error');
                }
                else if (err.status == 403) {
                    notify(err.data.mensaje, 'error');
                    setTimeout(function () {
                        $window.location = base_url + 'login/close';
                    }, 1500);
                }
            });
        }

        $scope.deshabilitar_convocatoria = function (convocatoria_id) {
            $http({
                method: "POST",
                url: api_url + "convocatorias/deshabilitar_convocatoria_by_id",
                params: { id: convocatoria_id }
            }).then(function (response) {
                notify(response.data.mensaje, 'success');
                get_convocatorias();
            }).catch(function (err) {
                if (err.status == 500) {
                    notify(err.data.mensaje, 'error');
                } else if (err.status == 400) {
                    notify(err.data.mensaje, 'error');
                }
                else if (err.status == 403) {
                    notify(err.data.mensaje, 'error');
                    setTimeout(function () {
                        $window.location = base_url + 'login/close';
                    }, 1500);
                }
            });
        }

        $scope.habilitar_convocatoria = function (convocatoria_id) {
            $http({
                method: "POST",
                url: api_url + "convocatorias/habilitar_convocatoria_by_id",
                params: { id: convocatoria_id }
            }).then(function (response) {
                notify(response.data.mensaje, 'success');
                get_convocatorias();
            }).catch(function (err) {
                if (err.status == 500) {
                    notify(err.data.mensaje, 'error');
                } else if (err.status == 400) {
                    notify(err.data.mensaje, 'error');
                }
                else if (err.status == 403) {
                    notify(err.data.mensaje, 'error');
                    setTimeout(function () {
                        $window.location = base_url + 'login/close';
                    }, 1500);
                }
            });
        }
    });

