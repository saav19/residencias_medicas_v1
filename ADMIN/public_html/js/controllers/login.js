var app = angular.module('app', []);

app.controller('LoginCtrl',
    function ($scope, $http, $window, $sce) {
        $scope.form = { usuario: "", password: "" };
        $scope.boton_inicio_session = false;
        $scope.mensaje = '';

        $scope.submit = function () {
            $scope.boton_inicio_session = true;
            var data = {};
            data.usuario = angular.copy($scope.form.usuario);
            data.password = hex_sha1(angular.copy($scope.form.password));
            $http({
                method: "POST",
                url: base_url + "index.php/login/is_user_exits",
                data: data
            }).then(function (response) {
                mensaje(true, response.data.mensaje);
                setTimeout(function () {
                    $window.location = base_url + 'main';
                }, 1500);
            }).catch(function (err) {
                console.log(err.data.mensaje);
                if (err.status == 500) {
                    mensaje(false, err.data.mensaje);
                    $scope.boton_inicio_session = false;
                } else if (err.status == 400) {
                    mensaje(false, err.data.mensaje);
                    $scope.boton_inicio_session = false;

                }
            });
        }
        
        function mensaje(flag, mensaje) {
            if (flag) {
                var mensajehtml = ' <p class="text-center p-3 mb-2 bg-success text-white">' + mensaje + '</p>'
            } else {
                var mensajehtml = ' <p class="text-center p-3 mb-2 bg-danger text-white">' + mensaje + '</p>'
            }
            $scope.mensaje = mensajehtml;

        }

        
        $scope.renderHtml = function (htmlCode) {
            return $sce.trustAsHtml(htmlCode);
        };
    }
);
