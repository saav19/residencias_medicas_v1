<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="<?= base_url() ?>css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/noty.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/fontawesome-all.min.css">
    <script type="text/javascript" src="<?= base_url(); ?>js/vendors/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>js/vendors/bootstrap.bundle.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>js/vendors/angular.min.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>js/vendors/noty.min.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>js/modules/notificaciones.js"></script>
    <script type="text/javascript">
        var base_url = "<?= base_url(); ?>index.php/";
        var api_url = base_url + "api/";
        <?php if ($template == 3 || $template == 2) { ?>
            var app = angular.module('app', ['jcs-autoValidate']);
        <?php } else { ?>
            var app = angular.module('app', []);
        <?php } ?>

    </script>
    <?php
    //switch para js y css
    switch ($template) {
        case 1:
            if ($this->session->session_type != 1) {
                echo '<script type="text/javascript" src="' . base_url() . 'js/controllers/aspirantes.js"></script>';
            } else {
                echo '<script type="text/javascript" src="' . base_url() . 'js/controllers/aspirantes_for_curso.js"></script>';
            }
            break;
        case 2:
            echo '<link href="' . base_url() . 'css/union-util.css" rel="stylesheet">';
            echo '<script type="text/javascript" src="' . base_url() . 'js/modules/jcs-auto-validate.min.js"></script>';
            echo '<script type="text/javascript" src="' . base_url() . 'js/controllers/convocatorias.js"></script>';
            break;
        case 3:
            echo '<link href="' . base_url() . 'css/union-util.css" rel="stylesheet">';
            echo '<script type="text/javascript" src="' . base_url() . 'js/controllers/change_password.js"></script>';
            echo '<script type="text/javascript" src="' . base_url() . 'js/vendors/sha1.min.js"></script>';
            echo '<script type="text/javascript" src="' . base_url() . 'js/modules/jcs-auto-validate.min.js"></script>';
            break;
        default:
            if ($this->session->session_type != 1) {
                echo '<script type="text/javascript" src="' . base_url() . 'js/controllers/aspirantes.js"></script>';
            } else {
                echo '<script type="text/javascript" src="' . base_url() . 'js/controllers/aspirantes_for_curso.js"></script>';
            }
            break;
    }
    ?>
    <title><?= $titulo; ?></title>
</head>

<body ng-app="app">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark rounded">
        <h2 class="navbar-brand mr-auto">RESIDENCIAS MÉDICAS</h2>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <?php if ($this->session->session_type != 1) { ?>
            <div class="collapse navbar-collapse text-center" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto ">
                    <li class="nav-item " ng-class="{'active': <?= $template; ?> == 1}">
                        <a class="nav-link" href="<?= base_url(); ?>index.php/main">Aspirantes</a>
                    </li>
                    <li class="nav-item " ng-class="{'active': <?= $template; ?> == 2}">
                        <a class="nav-link" href="<?= base_url(); ?>index.php/main/convocatorias">Convocatorias</a>
                    </li>
                </ul>
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item dropdown active">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown-2" role="button" data-toggle="dropdown" aria-expanded="false">
                            Usuario: <?= $this->session->user_name; ?>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown-2">
                            <a class="dropdown-item" href="<?= base_url(); ?>index.php/main/change_password">Cambiar Contraseña</a>
                            <a class="dropdown-item" href="<?= base_url(); ?>index.php/login/close">Salir</a>
                        </div>
                    </li>
                </ul>
            </div>
        <?php } else { ?>
            <div class="collapse navbar-collapse text-center" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item dropdown active">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown-2" role="button" data-toggle="dropdown" aria-expanded="false">
                            Usuario: <?= $this->session->user_name; ?>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown-2">
                            <a class="dropdown-item" href="<?= base_url(); ?>index.php/main/change_password">Cambiar Contraseña</a>
                            <a class="dropdown-item" href="<?= base_url(); ?>login/close">Salir</a>
                        </div>
                    </li>
                </ul>
            </div>
        <?php } ?>
    </nav>
    <?php
    switch ($template) {
        case 1:
            if ($this->session->session_type != 1) {
                require_once 'templates/aspirantes.html';
            } else {
                require_once 'templates/aspirantes_for_curso.html';
            }
            break;
        case 2:
            require_once 'templates/convocatorias.html';
            break;
        case 3:
            require_once 'templates/change_password.html';
            break;
        default:
            if ($this->session->session_type != 1) {
                require_once 'templates/aspirantes.html';
            } else {
                require_once 'templates/aspirantes_for_curso.html';
            }
            break;
    }

    ?>
</body>

</html>