<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="<?= base_url(); ?>css/bootstrap.min.css" rel="stylesheet">
    <title>Error::Session</title>
    <style type="text/css">
        body {
            Background-image: url('<?= base_url() ?>img/logohjm.png');
            background-repeat: repeat;
            background-size: 92px 92px;
        }

        .contendor_mensaje {
            background-color: rgba(255, 255, 255, 1);
            border: 1px solid;
            padding: 10px;
            box-shadow: 7px 5px 10px #888888;

        }

        html,
        body {
            height: 100%;
        }

        * {
            margin: 0;
            padding: 0;
        }

        .wrapper {
            height: 100%;
            width: 100%;
            display: table;
            overflow: hidden;
        }

        .contenedor {
            display: table-cell;
            width: 100%;
            vertical-align: middle;
            text-align: center;
        }

        .contenido {
            display: inline-block;
            background-color: #CCC;
            margin: 0 auto;
        }

        .contendor_mensaje div {
            padding-top: 5%;
            padding-bottom: 5%;
        }
    </style>
</head>

<body>
    <div class="wrapper">
        <div class="contenedor text-center text-warning">
            <div class="contenido">
                <div class="contendor_mensaje">
                    <div>
                        <h1>!!Sin accesso!!!</h1>
                    </div>
                    <div> <a href="<?= base_url() ?>main" class="btn btn-outline-warning">Volver</a></div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>