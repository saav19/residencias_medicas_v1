<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="<?= base_url(); ?>css/bootstrap.min.css" rel="stylesheet">
    <script type="text/javascript" src="<?= base_url(); ?>js/vendors/angular.min.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>js/vendors/sha1.min.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>js/controllers/login.js"></script>
    <script type="text/javascript">
        var base_url = "<?= base_url(); ?>index.php/";
        var api_url = base_url + "api/";
    </script>
    <title>Login::Residencias-Medicas</title>
</head>

<body ng-app="app">
    <section class="vh-100">
        <div class="container py-5 h-100">
            <div class="row d-flex align-items-center justify-content-center " ng-controller="LoginCtrl">
                <img src="<?= base_url(); ?>img/logo.png" alt="Hospital Juárez de México" width="50%" style="padding-top: 10px;">
                <div class="col-12 text-center">
                    <p class="h2" style="padding-top: 15px">Sistema De Registo A Residencias Medicas</p>
                    <div class="col-md-4 offset-md-4">
                        <form ng-submit="submit()">
                            <div class="form-group">
                                <label class="form-label" for="user">Usuario:</label>
                                <input required type="text" class="form-control" id="user" ng-model="form.usuario">
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="password">Contraseña:</label>
                                <input required type="password" class="form-control" id="password" ng-model="form.password">
                            </div>
                            <div style="text-align:center;">
                                <button ng-disabled="boton_inicio_session" style="justify-content: center; padding: 10px; background-color: #15325b; color: white;/*rgba(0,0,0,0.12); color: rgba(0,0,0,0.38);*/" class="btn" type="submit">Iniciar Sesión</button>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-4 offset-md-4" style="margin-top:1%;" ng-bind-html="renderHtml(mensaje)"></div>
                </div>
                <img src="<?= base_url(); ?>img/desarrollado_en_ciimeit.png" alt="Hospital Juárez de México" width="20%" style=" text-align: center;padding-bottom: 35px; ">
            </div>
        </div>
    </section>
</body>

</html>