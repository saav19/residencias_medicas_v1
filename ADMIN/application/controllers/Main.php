<?php
class MAIn extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        $this->load->helper('url');
       require_once APPPATH.'helpers/Sesiones_helper.php';
        $this->load->library('session');
    }
    public function index()
    {
        if (is_logged_in()) {
            $data = ['template' => 1, 'titulo' => 'Aspirantes'];
            $this->load->view('Main_view.php', $data);
        } else {
            $this->load->view('errores/sesiones/Not_session_view.php');
        }
    }
    public function convocatorias()
    {
        if (is_logged_in()) {
            if ($this->session->session_type != 1) {
                $data = ['template' => 2, 'titulo' => 'Convocatorias'];
                $this->load->view('Main_view.php', $data);
            } else {
                $this->load->view('errores/sesiones/Not_access_view.php');
            }
        } else {
            $this->load->view('errores/sesiones/Not_session_view.php');
        }
    }

    public function change_password()
    {
        if (is_logged_in()) {
            $data = ['template' => 3, 'titulo' => 'Cambio de contraseña'];
            $this->load->view('Main_view.php', $data);
        } else {
            $this->load->view('errores/sesiones/Not_session_view.php');
        }
    }
}
