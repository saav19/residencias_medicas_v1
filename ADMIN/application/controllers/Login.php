<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        $this->load->helper('url');
       require_once APPPATH.'helpers/Sesiones_helper.php';
        $this->load->library('session');
        $_POST = json_decode(file_get_contents('php://input'), true);
    }

    public function index()
    {
        $this->_destroy_session_previa();
        $this->load->view('Login_view');
    }

    public function is_user_exits()
    {
        if (!$this->_validate_form()) {
            return $this->output
                ->set_status_header('400')->set_content_type('application/json')->set_output(json_encode(array('mensaje' => validation_errors())));
        } else {
            $this->load->model('login_model');
            $usuario = $this->login_model->get_usuario($this->input->post('usuario'), $this->input->post('password'));
            if ($usuario === null) {
                return $this->output
                    ->set_status_header('500')->set_content_type('application/json')->set_output(json_encode(array('mensaje' => "El usuario u contraseña es incorrecto")));
            } else {;
                $session_data = array(
                    'session_id'  => $usuario['id'],
                    'user_name'  => $usuario['nombre'],
                    'session_type' => (int)$usuario['tipo_admin'],
                    'logged_in' => TRUE
                );
                $this->session->set_userdata($session_data);
                return $this->output
                    ->set_status_header('200')->set_content_type('application/json')->set_output(json_encode(array('mensaje' => "iniciando sesion")));
            }
        }
    }

    public function change_pass()
    {
        $this->_validate_session();
        $this->_validate_form_pass();
        $this->load->library('session');
        $this->load->model('login_model');
        $user_pass = $this->login_model->get_password_user_by_id($this->session->session_id);
        if ($this->input->post('password_actual') !== $user_pass) {
            $this->_response(['code' => 400, 'data' => ['mensaje' => 'Su contraseña actual es erronea.']]);
        }
        if (!$this->login_model->update($this->session->session_id, ['password' =>  $this->input->post('password') , 'updated_at' => date('Y-m-d H:i:s')])) {
            $this->_response(['code' => 500, 'data' => ['mensaje' => 'Ocurrio un problema.']]);
        }
        $this->_response(['code' => 200, 'data' => ['mensaje' => 'La contraseña se actualizo con exito.']]);
    }

    public function close()
    {
        header("Location:" . base_url());
        $this->session->unset_userdata("session_data");
        $this->session->sess_destroy();
        die();
    }
    private function _response($response)
	{
		$this->output
			->set_status_header($response['code'])
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response['data'], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
			->_display();
		exit;
	}
    private function _validate_form()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('', '');
        $this->form_validation->set_rules('usuario', 'Usuario', 'trim|required|max_length[50]');
        $this->form_validation->set_rules('password', 'Contraseña', 'trim|required');
        if (!$this->form_validation->run()) {
            return false;
        } else {
            return true;
        }
    }
    private function _validate_form_pass()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('password_actual', 'Contraseña Actual', 'trim|required');
		$this->form_validation->set_rules('password', 'Contraseña', 'trim|required');
		$this->form_validation->set_rules('confirmpassword', 'Confirmación de la contraseña', 'trim|required|matches[password]');
		if (!$this->form_validation->run()) {
			$this->_response(['code' => 400, 'data' => ['mensaje' => validation_errors()]]);
		}
	}

    private function _destroy_session_previa()
    {
        if (session_status() === PHP_SESSION_NONE) {;
            $this->session->unset_userdata("session_data");
            $this->session->sess_destroy();
        }
    }

    private function _validate_session()
	{
		if (!is_logged_in()) {
			$this->_response(['code' => 403, 'data' => ['mensaje' => 'No cuenta con una sesión activa.']]);
		}
	}
}
