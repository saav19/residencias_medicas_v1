<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Convocatorias extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('America/Mexico_City');
       require_once APPPATH.'helpers/Sesiones_helper.php';
		$this->load->library('session');
		$this->load->model('convocatorias_model');
		$_POST = json_decode(file_get_contents('php://input'), true);
	}
	public function get_all_convocatorias()
	{
		$this->_validate_session();
		$convocatororias =  $this->convocatorias_model->get_all_convocatorias();
		$this->_response(['code' => 200, 'data' => ['convocatorias' => $convocatororias]]);
	}
	public function get_convocatoria_by_id()
	{
		$this->_validate_session();
		$this->_validate_convocatoria_id();
		$convocatororia =  $this->convocatorias_model->get_convocatoria_by_id($this->input->get('id'));
		if ($convocatororia === NULL) {
			$this->_response(['code' => 400, 'data' => []]);
		}
		$this->_response(['code' => 200, 'data' => ['convocatoria' => $convocatororia]]);
	}

	function get_convocatorias_by_tipo_convocatoria_id()
	{
		$this->_validate_session();
		$convocatorias =  $this->convocatorias_model->get_convocatorias_by_tipo_convocatoria_id($this->input->post('tipo_convocatoria_id'));
		$this->_response(['code' => 200, 'data' => ['convocatorias' => $convocatorias]]);
	}

	public function add_convocatoria()
	{
		$this->_validate_session();
		$this->_validate_form();
		$this->db->trans_start();
		$id = $this->convocatorias_model->insert($this->_fill_data_convocatoria_add());
		if (!$id) {
			$this->_response(['code' => 500, 'data' => ['mensaje' => 'Ocurrio un problema, por favor intentelo mas tarde.']]);
		}
		$this->_insert_bitacora_convocatorias($id, 1);
		$this->db->trans_complete();
		$this->_response(['code' => 200, 'data' => ['mensaje' => 'Se creo la convocatoria con exito.']]);
	}
	public function edit_convocatoria()
	{
		$this->_validate_session();
		$this->_validate_convocatoria_id();
		$this->_validate_form();
		$this->db->trans_start();
		if (!$this->convocatorias_model->update($this->input->get('id'), $this->_fill_data_convocatoria_update())) {
			$this->_response(['code' => 500, 'data' => ['mensaje' => 'Ocurrio un problema, por favor intentelo mas tarde.']]);
		}
		$this->_insert_bitacora_convocatorias($this->input->get('id'), 2);
		$this->db->trans_complete();
		$this->_response(['code' => 200, 'data' => ['mensaje' => 'Se edito la convocatoria con exito.']]);
	}

	public function finish_convocatoria_by_id()
	{
		$this->_validate_session();
		$this->_validate_convocatoria_id();
		$this->_validate_status(-2);
		$this->db->trans_start();
		if (!$this->convocatorias_model->update($this->input->get('id'), ['status' => -2, 'updated_at' =>  date("Y-m-d H:i:s")])) {
			$this->_response(['code' => 500, 'data' => ['mensaje' => 'Ocurrio un problema, por favor intentelo mas tarde.']]);
		}
		$this->_insert_bitacora_convocatorias($this->input->get('id'), -1);
		$this->db->trans_complete();
		$this->_response(['code' => 200, 'data' => ['mensaje' => 'Se finalizo la convocatoria con exito.']]);
	}
	public function deshabilitar_convocatoria_by_id()
	{
		$this->_validate_session();
		$this->_validate_convocatoria_id();
		$this->_validate_status(-1);
		$this->db->trans_start();
		if (!$this->convocatorias_model->update($this->input->get('id'), ['status' => -1, 'updated_at' =>  date("Y-m-d H:i:s")])) {
			$this->_response(['code' => 500, 'data' => ['mensaje' => 'Ocurrio un problema, por favor intentelo mas tarde.']]);
		}
		$this->_insert_bitacora_convocatorias($this->input->get('id'), -2);
		$this->db->trans_complete();
		$this->_response(['code' => 200, 'data' => ['mensaje' => 'Se Deshabilito la convocatoria con exito.']]);
	}

	public function habilitar_convocatoria_by_id()
	{
		$this->_validate_session();
		$this->_validate_convocatoria_id();
		$this->_validate_status(1);
		$this->db->trans_start();
		if (!$this->convocatorias_model->update($this->input->get('id'), ['status' => 1, 'updated_at' =>  date("Y-m-d H:i:s")])) {
			$this->_response(['code' => 500, 'data' => ['mensaje' => 'Ocurrio un problema, por favor intentelo mas tarde.']]);
		}
		$this->_insert_bitacora_convocatorias($this->input->get('id'), 1);
		$this->db->trans_complete();
		$this->_response(['code' => 200, 'data' => ['mensaje' => 'Se Habilito la convocatoria con exito.']]);
	}

	public function _insert_bitacora_convocatorias($id, $accion)
	{
		$this->load->model('bitacora_convocatorias_model');
		$data_insert = [
			'administrador_id' => $this->session->session_id,
			'convocatoria_id' => $id,
			'accion' => $accion,
			'created_at' => date("Y-m-d H:i:s")
		];
		if (!$this->bitacora_convocatorias_model->insert($data_insert)) {
			$this->_response(['code' => 500, 'data' => ['mensaje' => "Ocurrio un error, por favor intentalo más tarde."]]);
		}
	}

	private function _response($response)
	{
		$this->output
			->set_status_header($response['code'])
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response['data'], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
			->_display();
		exit;
	}

	private function _fill_data_convocatoria_add()
	{
		$data_insert['nombre_convocatoria'] = mb_strtoupper($this->input->post('nombre_convocatoria'), 'UTF-8');
		$data_insert['tipo_convocatoria_id'] = $this->input->post('tipo_convocatoria_id');
		$data_insert['fecha_inicio'] = $this->input->post('fecha_inicio');
		$data_insert['fecha_termino'] = $this->input->post('fecha_termino');
		$data_insert['status'] = 1;
		$data_insert['created_at'] = date("Y-m-d H:i:s");
		return $data_insert;
	}
	private function _fill_data_convocatoria_update()
	{
		$data_insert['nombre_convocatoria'] = mb_strtoupper($this->input->post('nombre_convocatoria'), 'UTF-8');
		$data_insert['tipo_convocatoria_id'] = $this->input->post('tipo_convocatoria_id');
		$data_insert['fecha_inicio'] = $this->input->post('fecha_inicio');
		$data_insert['fecha_termino'] = $this->input->post('fecha_termino');
		$data_insert['status'] = 1;
		$data_insert['updated_at'] = date("Y-m-d H:i:s");
		return $data_insert;
	}

	//--------------validaciones formvalidation
	public function validateDate($horario)
	{
		$horario = substr($horario, 0, 10);
		$formato = "/^(\d{4})-(\d{2})-(\d{2})$/";
		if (!preg_match($formato, $horario)) {
			$this->form_validation->set_message('validateDate', 'El {field} tiene un formato incorrecto ' . $horario);
			return false;
		}
		return true;
	}

	public function date_greater_than($fecha_egreso, $fecha_ingreso)
	{
		$fecha_ingreso = date($fecha_ingreso);
		$fecha_egreso = date($fecha_egreso);

		if ($fecha_egreso < $fecha_ingreso) {
			$this->form_validation->set_message('date_greater_than', 'La fecha de inicio es mayor a la fecha de termino.');
			return FALSE;
		} else {
			return TRUE;
		}
	}

	private function _validate_form()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('', '');
		$this->form_validation->set_data($this->input->post());
		$this->form_validation->set_rules('nombre_convocatoria', 'Nombre', 'trim|required');
		$this->form_validation->set_rules('tipo_convocatoria_id', 'Tipo', 'trim|required|is_natural_no_zero|in_list[1,2,3]');
		$this->form_validation->set_rules('fecha_inicio', 'Fecha de inicio', 'trim|required|callback_validateDate');
		$this->form_validation->set_rules('fecha_termino', 'Fecha de termino', 'trim|required|callback_validateDate|callback_date_greater_than[' . $this->input->post('fecha_inicio') . ']');
		if (!$this->form_validation->run()) {
			$this->_response(['code' => 400, 'data' => ['mensaje' => validation_errors()]]);
		}
	}

	private function _validate_convocatoria_id()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('', '');
		$this->form_validation->set_data($this->input->get());
		$this->form_validation->set_rules('id', 'Id de la Convocatoria', 'trim|required|is_natural_no_zero');
		if (!$this->form_validation->run()) {
			$this->_response(['code' => 400, 'data' => ['mensaje' => validation_errors()]]);
		}
	}

	private function _validate_status($status_for)
	{
		$status = $this->convocatorias_model->get_status_convocatoria_by_id($this->input->get('id'));
		if (is_null($status)) {
			$this->_response(['code' => 500, 'data' => ['mensaje' => 'Esta convocatoria no existe.']]);
		}
		switch ($status_for) {
			case -2:
				if ($status == -2) {
					$this->_response(['code' => 400, 'data' => ['mensaje' => 'Esta convocatoria ya esta finalizada']]);
				}
				break;
			case -1:
				if ($status == -1) {
					$this->_response(['code' => 400, 'data' => ['mensaje' => 'Esta convocatoria ya esta Deshabilitada']]);
				}
				break;
			case 1:
				if ($status == 1) {
					$this->_response(['code' => 400, 'data' => ['mensaje' => 'Esta convocatoria ya esta Habilitada']]);
				}
				break;
			default:
				# code...
				break;
		}
	}

	private function _validate_session()
	{
		if (!is_logged_in()) {
			$this->_response(['code' => 403, 'data' => ['mensaje' => 'No cuenta con una sesión activa.']]);
		}
	}
}
