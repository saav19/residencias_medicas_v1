<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Registros extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
       require_once APPPATH.'helpers/Sesiones_helper.php';
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->model('registros_model');
        $_POST = json_decode(file_get_contents('php://input'), true);
    }
    public function get_registros()
    {
        $this->_validate_session();
        $this->_validate_form();
        $aspirantes = $this->registros_model->get_registro_id_by_convocatoria_curso_ids((int)$this->input->post("convocatoria_id"), (int)$this->input->post("curso_id"), (int)$this->input->post("nacionalidad"));
        foreach ($aspirantes as $key => &$aspirante) {
            $aspirante['nombre_completo'] = $aspirante['nombre'] . ' ' . $aspirante['apellido_1'] . ' ' . $aspirante['apellido_2'];
            unset(
                $aspirante['nombre'],
                $aspirante['apellido_1'],
                $aspirante['apellido_2']
            );
        }
        $this->_response(['code' => 200, 'data' => ['aspirantes' => $aspirantes]]);
    }

    public function get_datos_aspirante_by_registro_id()
    {
        $this->_validate_session();
        $this->_validate_registro_id();
        $registro = $this->registros_model->get_registro_by_id($this->input->post("registro_id"));
        $datos_personales = $this->_get_datos_generales($registro['aspirante_id']);
        $datos_particulares = $this->_get_datos_particulares($registro['datos_aspirante_id'], $datos_personales['extranjero']);
        $identificador = (!$datos_personales['extranjero']) ? $datos_personales['curp'] : $datos_personales['pasaporte'];
        $archivos = $this->_get_archivos($registro['convocatoria_id'], $registro['curso_id'], $registro['archivos_aspirante_id'], $identificador);
        $data = [
            'id' => $this->input->post("registro_id"),
            'datos_personales' => $datos_personales,
            'datos_particulares' => $datos_particulares,
            'archivos' => $archivos,
            'status' => (int)$registro['status']
        ];
        $this->_response(['code' => 200, 'data' => ['data' => $data]]);
    }

    public function acept_aspirante_by_registro_id()
    {
        $this->_validate_session();
        $this->_validate_registro_id();
        $this->_validate_status_registro();
        $this->db->trans_start();
        if (!$this->registros_model->update($this->input->post("registro_id"), ['status' => 3, 'updated_at' => date("Y-m-d H:i:s")])) {
            $this->_response(['code' => 500, 'data' => ['mensaje' => "Ocurrio un error, por favor intentalo más tarde."]]);
        }
        $this->_insert_bitacora_registros(3);
        $this->db->trans_complete();
        $this->_response(['code' => 200, 'data' => ['mensaje' => "Aspirante aceptado con exitó."]]);
    }
    public function rechazar_aspirante_by_registro_id()
    {
        $this->_validate_session();
        $this->_validate_registro_id();
        $this->_validate_status_registro();
        $this->db->trans_start();
        if (!$this->registros_model->update($this->input->post("registro_id"), ['status' => -1, 'updated_at' => date("Y-m-d H:i:s")])) {
            $this->_response(['code' => 500, 'data' => ['mensaje' => "Ocurrio un error, por favor intentalo más tarde."]]);
        }
        $this->_insert_bitacora_registros(-1);
        $this->db->trans_complete();
        $this->_response(['code' => 200, 'data' => ['mensaje' => "Aspirante rechazado con exitó."]]);
    }

    public function return_status_registrado_by_registro_id()
    {
        $this->_validate_session();
        $this->_validate_registro_id();
        $this->_validate_status_aceptado_or_rechazado();
        $this->db->trans_start();
        if (!$this->registros_model->update($this->input->post("registro_id"), ['status' => 2, 'updated_at' => date("Y-m-d H:i:s")])) {
            $this->_response(['code' => 500, 'data' => ['mensaje' => "Ocurrio un error, por favor intentalo más tarde."]]);
        }
        $this->_insert_bitacora_registros(2);
        $this->db->trans_complete();
        $this->_response(['code' => 200, 'data' => ['mensaje' => "Aspirante volivo a estar en registrado."]]);
    }

    public function downgrade_registro_by_registro_id()
    {
        $this->_validate_session();
        $this->_validate_registro_id();
        $this->db->trans_start();
        if (!$this->registros_model->update($this->input->post("registro_id"), ['status' => 1, 'updated_at' => date("Y-m-d H:i:s")])) {
            $this->_response(['code' => 500, 'data' => ['mensaje' => "Ocurrio un error, por favor intentalo más tarde."]]);
        }
        $this->_insert_bitacora_registros(1);
        $this->db->trans_complete();
        $this->_response(['code' => 200, 'data' => ['mensaje' => "Aspirante puede modificar de nuevo su registro."]]);
    }

    private function _get_datos_generales($aspirante_id)
    {
        $this->load->model('aspirantes_model');
        $datos = $this->aspirantes_model->get_aspirante_by_id($aspirante_id);
        $this->load->model('paises_model');
        $datos['pais'] = $this->paises_model->get_pais_by_id($datos['pais_id']);
        if (!is_null($datos)) {
            if ($datos['pais_id'] == 1) {
                $this->load->model('entidades_model');
                $datos['nacimiento_entidad'] = $this->entidades_model->get_entidad_by_id($datos['nacimiento_entidad_id']);
                $datos['extranjero'] = false;
                unset(
                    $datos['pasaporte'],
                    $datos['pais_id'],
                    $datos['nacimiento_entidad_id']
                );
            } else {
                $datos['extranjero'] = true;
                unset(
                    $datos['curp'],
                    $datos['pais_id'],
                    $datos['rfc'],
                    $datos['nacimiento_entidad_id']
                );
            }
            $datos['genero'] = $datos['genero'] !== 1 ? 'Masculino' : 'Femenino';
        }
        return $datos;
    }

    private function _get_datos_particulares($datos_aspirante_id, $extranjero)
    {
        $this->load->model('datos_aspirantes_model');
        if (!$extranjero) {
            $datos = $this->datos_aspirantes_model->get_datos_aspirante_nacional_by_id($datos_aspirante_id);
            if (!is_null($datos)) {
                $this->load->model('codigos_postales_model');
                $datos += $this->codigos_postales_model->get_direccion_by_codigo_postal_id($datos['codigo_postal_id']);
                unset($datos['codigo_postal_id']);
            }
        } else {
            $datos = $this->datos_aspirantes_model->get_datos_aspirante_extranjero_by_id($datos_aspirante_id);
        }
        if (!is_null($datos)) {
            $datos['estado_civil'] = $this->_get_estado_civil($datos['estado_civil']);
            $datos['grupo_sanguineo'] = $this->_get_grupo_sanguineo($datos['grupo_sanguineo']);
            $datos['factor_rh'] = $datos['factor_rh'] !== 1 ? 'Positivo' : 'Negativo';
            $datos['talla_uniforme_quirurgico'] = $this->_get_talla_uniforme_quirurgico($datos['talla_uniforme_quirurgico']);
            if (!is_null($datos['universidad_id']) && (int)$datos['universidad_id'] !== 73) {
                $this->load->model('universidades_model');
                $datos += $this->universidades_model->get_universidad_by_id($datos['universidad_id']);
            } else {
                $datos['universidad'] = $datos['universidad_otra_opcion'];
            }
            if (!is_null($datos['institucion_entidad_id'])) {
                $this->load->model('entidades_model');
                $datos['institucion_entidad'] = $this->entidades_model->get_entidad_by_id($datos['institucion_entidad_id']);
            } else {
                $datos['institucion_entidad'] = NULL;
            }
            unset(
                $datos['universidad_id'],
                $datos['universidad_otra_opcion'],
                $datos['institucion_entidad_id']
            );
        }
        return $datos;
    }

    private function _get_archivos($convocatoria_id, $curso_id, $archivos_aspirante_id, $identificador)
    {
        $data = [];
        $this->load->model('convocatorias_model');
        $convocatoria_name = $this->convocatorias_model->get_convocatoria_name_by_id($convocatoria_id);
        $this->load->model('cursos_model');
        $curso_name = $this->cursos_model->get_curso_name_by_id($curso_id);
        $this->load->model('archivos_aspirantes_model');
        $archivos = $this->archivos_aspirantes_model->get_archivos_aspirante_by_id($archivos_aspirante_id);
        if (!is_null($archivos)) {
            unset(
                $archivos['id'],
                $archivos['status'],
                $archivos['created_at'],
                $archivos['updated_at']
            );
            $dir = str_replace(' ', '', base_url() . '../../DOCUMENTOS/' . $convocatoria_name . '/' . $curso_name . '/' . $identificador);
            $data = $this->_array_files($dir, $archivos);
        }
        return $data;
    }

    private function _get_estado_civil($estado_civil)
    {
        $estado = '';
        switch ($estado_civil) {
            case 1:
                $estado = 'SOLTERO';
                break;
            case 2:
                $estado = 'CASADO';
                break;
            case 3:
                $estado = 'VIUDO';
                break;
            case 4:
                $estado = 'DIVORCIADO';
                break;
            case 5:
                $estado = 'UNION LIBRE';
                break;
        }
        return $estado;
    }

    private function _get_grupo_sanguineo($grupo_sanguineo)
    {
        $type_sangre = '';
        switch ($grupo_sanguineo) {
            case 1:
                $type_sangre = 'A';
                break;
            case 2:
                $type_sangre = 'B';
                break;
            case 3:
                $type_sangre = 'AB';
                break;
            case 4:
                $type_sangre = 'O';
                break;
        }
        return $type_sangre;
    }
    private function _get_talla_uniforme_quirurgico($talla_uniforme_quirurgico)
    {
        $talla = '';
        switch ($talla_uniforme_quirurgico) {
            case 1:
                $talla = 'Extra chica';
                break;
            case 2:
                $talla = 'Chica';
                break;
            case 3:
                $talla = 'Mediana';
                break;
            case 4:
                $talla = 'Grande';
                break;
            case 5:
                $talla = 'Extra grande';
                break;
        }
        return $talla;
    }

    private function _array_files($dir, $archivos)
    {
        $array = [];
        $count = 1;
        foreach ($archivos as $archivo) {
            if (!is_null($archivo)) {
                array_push($array, [
                    'file_name' => $this->_select_name($count),
                    'ruta' => $dir . '/' . $archivo
                ]);
            }
            $count++;
        }
        return $array;
    }
    private function _select_name($select)
    {
        $name = '';
        switch ($select) {
            case 1:
                $name = 'Constancia de seleccionado nacional';
                break;
            case 2:
                $name = 'Solicitud de ingreso';
                break;
            case 3:
                $name = 'Acta de nacimiento';
                break;
            case 4:
                $name = 'CURP';
                break;
            case 5:
                $name = 'RFC';
                break;
            case 6:
                $name = 'Pasaporte';
                break;
            case 7:
                $name = 'Identificacion oficial';
                break;
            case 8:
                $name = 'Cartilla militar';
                break;
            case 9:
                $name = 'Comprobante de domicilio';
                break;
            case 10:
                $name = 'Historial academico de licenciatura';
                break;
            case 11:
                $name = 'Terminación de internado';
                break;
            case 12:
                $name = 'Liberación de servicio social';
                break;
            case 13:
                $name = 'Título profesional';
                break;
            case 14:
                $name = 'Cédula profesional';
                break;
            case 15:
                $name = 'Título de especialista';
                break;
            case 16:
                $name = 'Cédula de especialista';
                break;
            case 17:
                $name = 'Aval académico de previos de posgrado';
                break;
            case 18:
                $name = 'Carta de motivos';
                break;
            case 19:
                $name = 'Dos Cartas de recomendación';
                break;
            case 20:
                $name = 'Certificado médico';
                break;
            case 21:
                $name = 'Cartilla de vacunación';
                break;
            case 22:
                $name = 'Curriculum vitae';
                break;
            case 23:
                $name = 'Constancia de Solvencia Económica';
                break;
            case 24:
                $name = 'Recibo de pago de examen psicométrico y psicológico';
                break;
            case 25:
                $name = 'Curso ATLS, ACLS, o PALMS';
                break;
            case 26:
                $name = 'Carta de buena conducta';
                break;
            case 27:
                $name = 'Carta de aceptación de lineamientos';
                break;
            case 28:
                $name = 'Evaluación psicométrica y psicológica';
                break;
            case 29:
                $name = 'Ficha de salud';
                break;
            case 30:
                $name = 'Fotografía';
                break;
            case 31:
                $name = 'Pago de inscripción';
                break;

                // default:
                //     $name = 'INE';
                //     break;
        }
        return $name;
    }

    public function _insert_bitacora_registros($accion)
    {
        $this->load->model('bitacora_registros_model');
        $data_insert = [
            'administrador_id' => $this->session->session_id,
            'registro_id' => $this->input->post("registro_id"),
            'accion' => $accion,
            'created_at' => date("Y-m-d H:i:s")
        ];
        if (!$this->bitacora_registros_model->insert($data_insert)) {
            $this->_response(['code' => 500, 'data' => ['mensaje' => "Ocurrio un error, por favor intentalo más tarde."]]);
        }
    }

    private function _response($response)
    {
        $this->output
            ->set_status_header($response['code'])
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response['data'], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
            ->_display();
        exit;
    }
    private function _validate_form()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('', '');
        $this->form_validation->set_rules('convocatoria_id', 'Convocatoria', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('curso_id', 'Curso', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('nacionalidad', 'Nacionalidad', 'trim|required|is_natural_no_zero|in_list[1,2]');
        if (!$this->form_validation->run()) {
            $this->_response(['code' => 400, 'data' => ['mensaje' => validation_errors()]]);
        }
    }
    private function _validate_registro_id()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('', '');
        $this->form_validation->set_rules('registro_id', 'Aspirante', 'trim|required|is_natural_no_zero');
        if (!$this->form_validation->run()) {
            $this->_response(['code' => 400, 'data' => ['mensaje' => validation_errors()]]);
        }
    }

    private function _validate_status_registro()
    {
        $status = $this->registros_model->get_status_registro_by_id($this->input->post("registro_id"));
        if ($status == 3) {
            $this->_response(['code' => 400, 'data' => ['mensaje' => "Este aspirante ya fue aceptado."]]);
        }
        if ($status == -1) {
            $this->_response(['code' => 400, 'data' => ['mensaje' => "Este aspirante ya fue rechazado."]]);
        }
    }
    private function _validate_status_aceptado_or_rechazado()
    {
        $status = $this->registros_model->get_status_registro_by_id($this->input->post("registro_id"));
        if ($status == 2) {
            $this->_response(['code' => 400, 'data' => ['mensaje' => "Este aspirante no se puede modificar."]]);
        }
    }

    private function _validate_session()
    {
        if (!is_logged_in()) {
            $this->_response(['code' => 403, 'data' => ['mensaje' => 'No cuenta con una sesión activa.']]);
        }
    }
}
