<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Cursos extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
       require_once APPPATH.'helpers/Sesiones_helper.php';
		$this->load->library('session');
		$this->load->model('cursos_model');
		$_POST = json_decode(file_get_contents('php://input'), true);
	}

	public function get_cursos_by_tipo_convocatoria_id()
	{
		$this->_validate_session();
		$cursos =  $this->cursos_model->get_cursos_by_tipo_convocatoria_id($this->input->post('tipo_convocatoria_id'));
		$this->_response(['code' => 200, 'data' => ['cursos' => $cursos]]);
	}

	public function get_curso_by_id()
	{
		$this->_validate_session();
		$curso =  $this->cursos_model->get_curso_by_id($this->session->curso_id);
		if ($curso === NULL) {
			$this->_response(['code' => 500, 'data' => ['mensaje' => "Ocurrio un erro, por favor intentelo más tarde."]]);
		}
		$this->_response(['code' => 200, 'data' => ['curso' => $curso]]);
	}

	public function get_all_cursos_by_administrador_id()
	{
		$this->_validate_session();
		$this->load->model('administradores_cursos_model');
		$array_cursos_usuarios = $this->administradores_cursos_model->get_administrador_cursos_by_administrador_id($this->session->session_id);
		$this->_response(['code' => 200, 'data' => ['cursos' => $array_cursos_usuarios]]);
	}


	private function _response($response)
	{
		$this->output
			->set_status_header($response['code'])
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response['data'], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
			->_display();
		exit;
	}

	private function _validate_session()
	{
		if (!is_logged_in()) {
			$this->_response(['code' => 403, 'data' => ['mensaje' => 'No cuenta con una sesión activa.']]);
		}
	}
}
