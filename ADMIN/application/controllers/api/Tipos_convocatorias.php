<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tipos_convocatorias extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
       require_once APPPATH.'helpers/Sesiones_helper.php';
        $this->load->library('session');
        $this->load->model('tipos_convocatorias_model');
        $_POST = json_decode(file_get_contents('php://input'), true);
    }

    public function get_all_tipos_convocatorias()
	{
		$this->_validate_session();
		$tipos_convocatororias =  $this->tipos_convocatorias_model->get_all_tipos_convocatorias();
		$this->_response(['code' => 200, 'data' => ['tipos_convocatorias' => $tipos_convocatororias]]);
	}

    private function _response($response)
    {
        $this->output
            ->set_status_header($response['code'])
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response['data'], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
            ->_display();
        exit;
    }


    private function _validate_session()
    {
        if (!is_logged_in()) {
            $this->_response(['code' => 403, 'data' => ['mensaje' => 'No cuenta con una sesión activa.']]);
        }
    }
}
