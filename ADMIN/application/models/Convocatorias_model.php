<?php
class Convocatorias_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->table = 'convocatorias';
    }

    public function get_all_convocatorias()
    {
        $this->db->select('id, nombre_convocatoria, tipo_convocatoria_id, fecha_inicio, fecha_termino, status');
        $this->db->from($this->table);
        $this->db->where_in('status', array(-1, 1));
        $result = $this->db->get();
        return $result->result_array();
    }
    public function get_convocatoria_by_id($id)
    {
        $this->db->select('id, nombre_convocatoria, tipo_convocatoria_id, fecha_inicio, fecha_termino');
        $this->db->from($this->table);
        $this->db->where('id', $id);
        $this->db->where_in('status', array(-1,1));
        $this->db->limit(1);
        $result =  $this->db->get();
        $consulta =  $result->row_array();
        return $consulta !== NULL ? $consulta : NULL;
    }
    public function get_convocatoria_name_by_id($id)
    {
        $this->db->select('nombre_convocatoria');
        $this->db->from($this->table);
        $this->db->where('id', $id);
        $this->db->where('status', 1);
        $this->db->limit(1);
        $result =  $this->db->get();
        $consulta =  $result->row_array();
        return $consulta !== NULL ? $consulta['nombre_convocatoria'] : NULL;
    }

    public function get_status_convocatoria_by_id($id)
    {
        $this->db->select('status');
        $this->db->from($this->table);
        $this->db->where('id', $id);
        $this->db->where_in('status', array(-1,1));
        $this->db->limit(1);
        $result =  $this->db->get();
        $consulta =  $result->row_array();
        return $consulta !== NULL ? $consulta['status'] : NULL;
    }
    public function get_tipo_convocatoria_id_by_id($id)
    {
        $this->db->select('tipo_convocatoria_id');
        $this->db->from($this->table);
        $this->db->where('id', $id);
        $this->db->where('status', 1);
        $this->db->limit(1);
        $result =  $this->db->get();
        $consulta =  $result->row_array();
        return $consulta !== NULL ? $consulta : NULL;
    }

    public function get_convocatorias_by_tipo_convocatoria_id($tipo_convocatoria_id)
    {
        $this->db->select('id, nombre_convocatoria, tipo_convocatoria_id, status');
        $this->db->from($this->table);
        $this->db->where('tipo_convocatoria_id', $tipo_convocatoria_id);
        $this->db->where_in('status', array(-1,1));
        $this->db->order_by('nombre_convocatoria');
        $result =  $this->db->get();
        return $result->result_array();
    }
    public function insert($data)
    {
        if ($this->db->insert($this->table, $data)) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    public function update($id, $data)
    {
        if ($this->db->update($this->table, $data, array('id' => $id))) {
            return $id;
        } else {
            return False;
        }
    }
}
