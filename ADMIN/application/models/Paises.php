<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Paises extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('paises_model');
	}
	public function get_all_paises()
	{

		$paises =  $this->paises_model->get_all_paises();
		$this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode(['paises' => $paises], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
			->_display();
		exit;
	}
}
