<?php
class Paises_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->table = 'paises';
    }

    public function get_pais_by_id($id)
    {
        $this->db->select('pais');
        $this->db->from($this->table);
        $this->db->where('status', 1);
        $this->db->where('id', $id);
        $this->db->limit(1);
        $result =  $this->db->get();
        $consulta =  $result->row_array();
        return $consulta !== NULL ? $consulta['pais'] : NULL;
    }
}
