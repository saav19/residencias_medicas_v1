<?php
class Archivos_aspirantes_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->table = 'archivos_aspirantes';
    }
    public function get_archivos_aspirante_by_id($id)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where('id', $id);
        $this->db->where('status', 1);
        $this->db->limit(1);
        $result =  $this->db->get();
        $consulta =  $result->row_array();
        return $consulta !== NULL ? $consulta : NULL;
    }
}
