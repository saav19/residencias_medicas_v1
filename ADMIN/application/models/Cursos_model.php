<?php
class Cursos_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->table = 'cursos';
    }

    public function get_cursos_by_tipo_convocatoria_id($tipo_convocatoria_id)
    {
        $this->db->select('id, tipo_convocatoria_id, curso,status');
        $this->db->from($this->table);
        $this->db->where('tipo_convocatoria_id', $tipo_convocatoria_id);
        $this->db->where('status', 1);
        $this->db->order_by('curso');
        $result =  $this->db->get();
        return $result->result_array();
    }
    public function get_curso_by_id($id)
    {
        $this->db->select('id, tipo_convocatoria_id, curso, status');
        $this->db->from($this->table);
        $this->db->where('id', $id);
        $this->db->where('status', 1);
        $this->db->limit(1);
        $result =  $this->db->get();
        $consulta =  $result->row_array();
        return $consulta !== NULL ? $consulta : NULL;
    }

    public function get_curso_name_by_id($id)
    {
        $this->db->select('curso');
        $this->db->from($this->table);
        $this->db->where('id', $id);
        $this->db->where('status', 1);
        $this->db->limit(1);
        $result =  $this->db->get();
        $consulta =  $result->row_array();
        return $consulta !== NULL ? $consulta['curso'] : NULL;
    }
}