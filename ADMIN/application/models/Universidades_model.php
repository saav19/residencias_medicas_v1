<?php
class Universidades_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->table = 'universidades';
    }

    public function get_universidad_by_id($id)
    {
        $this->db->select('universidad');
        $this->db->from($this->table);
        $this->db->where('id', $id);
        //$this->db->where('status', 1);
        $this->db->limit(1);
        $result =  $this->db->get();
        $consulta =  $result->row_array();
        return $consulta !== NULL ? $consulta : NULL;
    }
}
