<?php
class Datos_aspirantes_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->table = 'Datos_aspirantes';
    }

    public function get_datos_aspirante_nacional_by_id($id)
    {
        $this->db->select('codigo_postal_id, institucion_entidad_id, universidad_id, correo_electronico, estado_civil, domicilio_calle, domicilio_numero_exterior, domicilio_numero_interior, domicilio_entre_calle_1, domicilio_entre_calle_2, telefono_fijo, telefono_movil, grupo_sanguineo, factor_rh, talla_bata, talla_uniforme_quirurgico, talla_pantalon, talla_zapatos, folio_enarm, cedula_profesional, universidad_otra_opcion, hospital_procedencia, ano_egreso_licenciatura, promedio_licenciatura');
        $this->db->from($this->table);
        $this->db->where('id', $id);
        $this->db->where('status', 1);
        $this->db->limit(1);
        $result =  $this->db->get();
        $consulta =  $result->row_array();
        return $consulta !== NULL ? $consulta : NULL;
    }

    public function get_datos_aspirante_extranjero_by_id($id)
    {
        $this->db->select('institucion_entidad_id, universidad_id, correo_electronico, estado_civil, domicilio_extranjero, telefono_fijo, telefono_movil, grupo_sanguineo, factor_rh, talla_bata, talla_uniforme_quirurgico, talla_pantalon, talla_zapatos, folio_enarm, cedula_profesional, universidad_otra_opcion, hospital_procedencia, ano_egreso_licenciatura, promedio_licenciatura');
        $this->db->from($this->table);
        $this->db->where('id', $id);
        $this->db->where('status', 1);
        $this->db->limit(1);
        $result =  $this->db->get();
        $consulta =  $result->row_array();
        return $consulta !== NULL ? $consulta : NULL;
    }
}
