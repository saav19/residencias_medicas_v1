<?php
class Tipos_convocatorias_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->table = 'tipos_convocatorias';
    }

    public function get_all_tipos_convocatorias()
    {
        $this->db->select('id, tipo');
        $this->db->from($this->table);
        $this->db->where('status', 1);
        $result = $this->db->get();
        return $result->result_array();
    }
}
