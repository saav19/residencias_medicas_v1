<?php
class Administradores_cursos_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->table = 'administradores_cursos';
    }

    public function get_administrador_cursos_by_administrador_id($administrador_id)
    {
        $this->db->select('cursos.id, cursos.curso, cursos.tipo_convocatoria_id');
        $this->db->from($this->table);
        $this->db->join('cursos', 'cursos.id = administradores_cursos.curso_id','left');
        $this->db->where('administradores_cursos.administrador_id', $administrador_id);
        $this->db->where('administradores_cursos.status', 1);
        $result =  $this->db->get();
        return $result->result_array();
    }


}
