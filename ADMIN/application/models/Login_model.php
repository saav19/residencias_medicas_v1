<?php
class Login_model extends CI_model
{
    public function __construct()
    {
        $this->load->database();
        $this->table = 'administradores';
    }
    public function get_usuario($usuario, $password)
    {
        $this->db->select('id, nombre, tipo_admin, status');
        $this->db->from($this->table);
        $this->db->where('usuario', $usuario);
        $this->db->where('password', $password);
        $this->db->where('status', 1);
        $this->db->limit(1);
        $result =  $this->db->get();
        $consulta =  $result->row_array();
        return $consulta !== NULL ? $consulta : NULL;
    }
    
    public function get_password_user_by_id($id)
    {
        $this->db->select('password');
        $this->db->from($this->table);
        $this->db->where('id', $id);
        $this->db->where('status', 1);
        $this->db->limit(1);
        $result =  $this->db->get();
        $consulta =  $result->row_array();
        return $consulta !== NULL ? $consulta['password'] : NULL;
    }

    public function update($id, $data)
    {
        $result = $this->db->update($this->table, $data, ['id' => $id]);
        if ($result) {
            return $id;
        } else {
            return FALSE;
        }
    }
}
