<?php
class Registros_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->table = 'registros';
    }
    public function get_registro_id_by_convocatoria_curso_ids($convocatoria_id, $curso_id, $modalidad)
    {
        $this->db->select('registros.id, aspirantes.nombre, aspirantes.apellido_1, aspirantes.apellido_2, datos_aspirantes.correo_electronico, registros.status, registros.created_at');
        $this->db->from($this->table);
        $this->db->join('aspirantes', 'aspirantes.id = registros.aspirante_id', 'left');
        $this->db->join('datos_aspirantes', 'datos_aspirantes.id = registros.datos_aspirante_id', 'left');
        $this->db->where(array('registros.convocatoria_id' => $convocatoria_id, 'registros.curso_id' => $curso_id));
        $this->db->where_in('registros.status', array(2, 3, -1));
        if ($modalidad === 1) {
            $this->db->where('aspirantes.pais_id', 1);
        } else {
            $this->db->where('aspirantes.pais_id !=', 1);
        }
        $this->db->order_by('registros.updated_at', 'DESC');
        $result =  $this->db->get();
        return $result->result_array();
    }

    public function get_registro_by_id($registro_id)
    {
        $this->db->select('convocatoria_id, curso_id, aspirante_id, datos_aspirante_id, archivos_aspirante_id, status');
        $this->db->from($this->table);
        $this->db->where('id', $registro_id);
        $this->db->where_in('status', array(2,3,-1));
        $this->db->limit(1);
        $result =  $this->db->get();
        $consulta =  $result->row_array();
        return $consulta !== NULL ? $consulta : NULL;
    }
    public function get_status_registro_by_id($registro_id)
    {
        $this->db->select('status');
        $this->db->from($this->table);
        $this->db->where('id', $registro_id);
        //$this->db->where('status', 2);
        $this->db->limit(1);
        $result =  $this->db->get();
        $consulta =  $result->row_array();
        return $consulta !== NULL ? $consulta['status'] : NULL;
    }

    public function update($id, $data)
    {
        if ($this->db->update($this->table, $data, array('id' => $id))) {
            return $id;
        } else {
            return False;
        }
    }
}
