<?php
class Codigos_postales_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->table = 'codigos_postales';
    }

    
    public function get_direccion_by_codigo_postal_id($id)
    {
        $this->db->select('codigo_postal, colonia, municipio, estado');
        $this->db->from($this->table);
        $this->db->where('id', $id);
        $this->db->limit(1);
        $result =  $this->db->get();
        $consulta =  $result->row_array();
        return $consulta !== NULL? $consulta: NULL;
    }
}
