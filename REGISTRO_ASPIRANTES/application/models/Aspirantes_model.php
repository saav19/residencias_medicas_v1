<?php
class Aspirantes_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->table = 'aspirantes';
    }

    public function get_aspirante_curp_by_id($id)
    {
        $this->db->select('curp');
        $this->db->from($this->table);
        $this->db->where('id', $id);
        $this->db->where('status', 1);
        $this->db->limit(1);
        $result =  $this->db->get();
        $consulta =  $result->row_array();
        return $consulta !== NULL ? $consulta : NULL;
    }
    public function get_aspirante_pasaporte_by_id($id)
    {
        $this->db->select('pasaporte');
        $this->db->from($this->table);
        $this->db->where('id', $id);
        $this->db->where('status', 1);
        $this->db->limit(1);
        $result =  $this->db->get();
        $consulta =  $result->row_array();
        return $consulta !== NULL ? $consulta : NULL;
    }

    public function get_aspirante_by_curp($curp)
    {
        $this->db->select('id, pais_id, nacimiento_entidad_id, nombre, apellido_1, apellido_2, genero, fecha_nacimiento, curp, rfc,status');
        $this->db->from($this->table);
        $this->db->where('curp', $curp);
        $this->db->where('status', 1);
        $this->db->limit(1);
        $result =  $this->db->get();
        $consulta =  $result->row_array();
        return $consulta !== NULL ? $consulta : NULL;
    }
    public function get_aspirante_by_pasaporte($pasaporte)
    {
        $this->db->select('id, pais_id, nacimiento_entidad_id, nombre, apellido_1, apellido_2, genero, fecha_nacimiento, pasaporte, status');
        $this->db->from($this->table);
        $this->db->where('pasaporte', $pasaporte);
        $this->db->where('status', 1);
        $this->db->limit(1);
        $result =  $this->db->get();
        $consulta =  $result->row_array();
        return $consulta !== NULL ? $consulta : NULL;
    }

    public function insert($data)
    {
        if ($this->db->insert($this->table, $data)) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    public function update($id, $data)
    {
        if ($this->db->update($this->table, $data, array('id' => $id))) {
            return $id;
        } else {
            return False;
        }
    }
}
