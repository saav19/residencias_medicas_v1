<?php
class Convocatorias_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->table = 'convocatorias';
    }

    public function get_all_convocatorias()
    {
        $this->db->select('id, nombre_convocatoria, tipo_convocatoria_id, fecha_inicio, fecha_termino, status');
        $this->db->from($this->table);
        $this->db->where('status', 1);
        $result = $this->db->get();
        return $result->result_array();
    }
    public function get_convocatoria_by_id($id)
    {
        $this->db->select('id, nombre_convocatoria, tipo_convocatoria_id, status');
        $this->db->from($this->table);
        $this->db->where('id', $id);
        $this->db->where('status', 1);
        $this->db->limit(1);
        $result =  $this->db->get();
        $consulta =  $result->row_array();
        return $consulta !== NULL ? $consulta : NULL;
    }

    public function get_tipo_convocatoria_id_by_id($id)
    {
        $this->db->select('tipo_convocatoria_id');
        $this->db->from($this->table);
        $this->db->where('id', $id);
        $this->db->where('status', 1);
        $this->db->limit(1);
        $result =  $this->db->get();
        $consulta =  $result->row_array();
        return $consulta !== NULL ? $consulta : NULL;
    }
}