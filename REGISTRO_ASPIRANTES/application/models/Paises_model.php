<?php
class Paises_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->table = 'paises';
    }

    public function get_all_paises()
    {
        $this->db->select('id, pais, pais_clave, status');
        $this->db->from($this->table);
        $this->db->where('status', 1);
        $this->db->order_by('id');
        $result = $this->db->get();
        return $result->result_array();
    }
}
