<?php
class Registros_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->table = 'registros';
    }
    public function insert($data)
    {
        if ($this->db->insert($this->table, $data)) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }
    public function update($id, $data)
    {
        if ($this->db->update($this->table, $data, array('id' => $id))) {
            return $id;
        } else {
            return False;
        }
    }


    public function get_registro_id_by_aspirante_convocatoria_curso_ids($aspirante_id, $convocatoria_id, $curso_id)
    {
        $this->db->select('id, status');
        $this->db->from($this->table);
        $this->db->where(array('aspirante_id' => $aspirante_id, 'convocatoria_id' => $convocatoria_id, 'curso_id' => $curso_id));
        $this->db->limit(1);
        $result =  $this->db->get();
        $consulta =  $result->row_array();
        return $consulta !== NULL ? $consulta : NULL;
    }

    public function get_datos_aspirante_id_by_registro_id($registro_id)
    {
        $this->db->select('datos_aspirante_id');
        $this->db->from($this->table);
        $this->db->where('id', $registro_id);
        $this->db->where('status', 1);
        $this->db->limit(1);
        $result =  $this->db->get();
        $consulta =  $result->row_array();
        return $consulta !== NULL ? $consulta : NULL;
    }
    public function get_archivos_aspirante_id_by_registro_id($registro_id)
    {
        $this->db->select('archivos_aspirante_id');
        $this->db->from($this->table);
        $this->db->where('id', $registro_id);
        $this->db->where('status', 1);
        $this->db->limit(1);
        $result =  $this->db->get();
        $consulta =  $result->row_array();
        return $consulta !== NULL ? $consulta['archivos_aspirante_id'] : NULL;
    }
}
