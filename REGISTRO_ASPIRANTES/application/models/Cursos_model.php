<?php
class Cursos_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->table = 'cursos';
    }

    public function get_cursos_by_tipo_convocatoria_id($tipo_convocatoria_id)
    {
        $this->db->select('id, tipo_convocatoria_id, curso,status');
        $this->db->from($this->table);
        $this->db->where('tipo_convocatoria_id', $tipo_convocatoria_id);
        $this->db->where('status', 1);
        $this->db->order_by('curso');
        $result =  $this->db->get();
        return $result->result_array();
    }
    public function get_curso_by_id($id)
    {
        $this->db->select('cursos.id, cursos.tipo_convocatoria_id, cursos.curso, cursos.status, administradores.nombre as profesor_titular');
        $this->db->from($this->table);
        $this->db->join('administradores_cursos', 'administradores_cursos.curso_id = cursos.id', 'left');
        $this->db->join('administradores', 'administradores.id = administradores_cursos.administrador_id', 'left');
        $this->db->where('cursos.id', $id);
        $this->db->where('cursos.status', 1);
        $this->db->limit(1);
        $result =  $this->db->get();
        $consulta =  $result->row_array();
        return $consulta !== NULL ? $consulta : NULL;
    }
}
