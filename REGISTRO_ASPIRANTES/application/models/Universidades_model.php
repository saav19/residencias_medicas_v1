<?php
class Universidades_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->table = 'universidades';
    }

    public function get_all_universidades()
    {
        $this->db->select('id, universidad');
        $this->db->from($this->table);
        $result = $this->db->get();
        return $result->result_array();
    }
}
