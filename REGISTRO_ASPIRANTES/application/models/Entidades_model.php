<?php
class Entidades_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->table = 'entidades';
    }

    public function get_all_entidades()
    {
        $this->db->select('id, estado, status');
        $this->db->from($this->table);
        $this->db->where('status', 1);
        $result = $this->db->get();
        return $result->result_array();
    }
}
