<?php
class Archivos_aspirantes_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->table = 'archivos_aspirantes';
    }
    public function get_archivos_aspirante_by_id($id)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where('id', $id);
        $this->db->where('status', 1);
        $this->db->limit(1);
        $result =  $this->db->get();
        $consulta =  $result->row_array();
        return $consulta !== NULL ? $consulta : NULL;
    }
    public function insert($data)
    {
        if ($this->db->insert($this->table, $data)) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }
    public function update($id, $data)
    {
        if ($this->db->update($this->table, $data, array('id' => $id))) {
            return $id;
        } else {
            return False;
        }
    }
}
