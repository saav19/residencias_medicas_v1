<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="<?= base_url() ?>css/bootstrap.min.css" rel="stylesheet" />
	<link href="<?= base_url() ?>css/noty.css" rel="stylesheet" />
	<script src="<?= base_url(); ?>js/vendors/angular.min.js"></script>
	<script src="<?= base_url(); ?>js/vendors/noty.min.js"></script>
	<script src="<?= base_url(); ?>js/modules/notificaciones.js"></script>
	<script type="text/javascript">
		var base_url = "<?= base_url(); ?>index.php/";
		var api_url = base_url + "api/";
		var app = angular.module('app', []);
	</script>
	<?php
	switch ($template) {
		case 1: ?>
			<script src="<?= base_url(); ?>js/controllers/bienvenida.js"></script>
		<?php break;
		case 2: ?>
			<script src="<?= base_url(); ?>js/controllers/cabecera.js"></script>
			<script src="<?= base_url(); ?>js/controllers/form_general/datos_generales_nacional.js"></script>
		<?php break;
		case 3: ?>
			<script src="<?= base_url(); ?>js/controllers/cabecera.js"></script>
			<script src="<?= base_url(); ?>js/controllers/form_general/datos_generales_extranjero.js"></script>
		<?php break;
		case 4: ?>
			<script src="<?= base_url(); ?>js/controllers/cabecera.js"></script>
			<script src="<?= base_url(); ?>js/controllers/form_particular/datos_particulares_directa_nacional.js"></script>
		<?php break;
		case 5: ?>
			<script src="<?= base_url(); ?>js/controllers/cabecera.js"></script>
			<script src="<?= base_url(); ?>js/controllers/form_particular/datos_particulares_directa_extranjero.js"></script>
		<?php break;
		case 6: ?>
			<script src="<?= base_url(); ?>js/controllers/cabecera.js"></script>
			<script src="<?= base_url(); ?>js/controllers/form_particular/datos_particulares_indirecta_nacional.js"></script>
		<?php break;
		case 7: ?>
			<script src="<?= base_url(); ?>js/controllers/cabecera.js"></script>
			<script src="<?= base_url(); ?>js/controllers/form_particular/datos_particulares_indirecta_extranjero.js"></script>
		<?php break;
		case 8: ?>
			<script src="<?= base_url(); ?>js/controllers/cabecera.js"></script>
			<script src="<?= base_url(); ?>js/controllers/form_particular/datos_particulares_alta_nacional.js"></script>
		<?php break;
		case 9: ?>
			<script src="<?= base_url(); ?>js/controllers/cabecera.js"></script>
			<script src="<?= base_url(); ?>js/controllers/form_particular/datos_particulares_alta_extranjero.js"></script>
		<?php break;
		case 10:
		case 11:
		case 12:
		case 13:
		case 14:
		case 15: ?>
			<script type="text/javascript" src="<?= base_url(); ?>js/vendors/bootstrap.min.js"></script>
			<script type="text/javascript" src="<?= base_url(); ?>js/vendors/jquery-3.6.0.min.js"></script>
			<script type="text/javascript" src="<?= base_url(); ?>js/vendors/bootstrap.bundle.js"></script>
			<script src="<?= base_url(); ?>js/controllers/cabecera.js"></script>
			<script src="<?= base_url(); ?>js/controllers/archivos.js"></script>
			<link href="<?= base_url() ?>css/normalize.css" rel="stylesheet">
			<link href="<?= base_url() ?>css/component.css" rel="stylesheet">
			<link href="<?= base_url() ?>css/style.css" rel="stylesheet">
	<?php break;
		default:
			break;
	}
	?>
	<title><?= $document_title; ?></title>
</head>

<body ng-app="app">

	<?php

	switch ($template) {
		case 1:
			require_once  'templates/cabecera.html';
			require_once  'templates/bienvenida.html';
			break;
		case 2:
			require_once  'templates/cabecera_form.html';
			require_once  'templates/form_general/form_datos_generales_nacional.html';
			break;
		case 3:
			require_once  'templates/cabecera_form.html';
			require_once  'templates/form_general/form_datos_generales_extranjero.html';
			break;
		case 4:
			require_once  'templates/cabecera_form.html';
			require_once  'templates/form_particular/form_datos_particulares_directa_nacional.html';
			break;
		case 5:
			require_once  'templates/cabecera_form.html';
			require_once  'templates/form_particular/form_datos_particulares_directa_extranjero.html';
			break;
		case 6:
			require_once  'templates/cabecera_form.html';
			require_once  'templates/form_particular/form_datos_particulares_indirecta_nacional.html';
			break;
		case 7:
			require_once  'templates/cabecera_form.html';
			require_once  'templates/form_particular/form_datos_particulares_indirecta_extranjero.html';
			break;
		case 8:
			require_once  'templates/cabecera_form.html';
			require_once  'templates/form_particular/form_datos_particulares_alta_nacional.html';
			break;
		case 9:
			require_once  'templates/cabecera_form.html';
			require_once  'templates/form_particular/form_datos_particulares_alta_extranjero.html';
			break;
		case 10:
			require_once  'templates/cabecera_form.html';
			require_once  'templates/form_archivos/form_archivos_directa_nacional.html';
			break;
		case 11:
			require_once  'templates/cabecera_form.html';
			require_once  'templates/form_archivos/form_archivos_directa_extranjero.html';
			break;
		case 12:
			require_once  'templates/cabecera_form.html';
			require_once  'templates/form_archivos/form_archivos_indirecta_nacional.html';
			break;
		case 13:
			require_once  'templates/cabecera_form.html';
			require_once  'templates/form_archivos/form_archivos_indirecta_extranjero.html';
			break;
		case 14:
			require_once  'templates/cabecera_form.html';
			require_once  'templates/form_archivos/form_archivos_alta_nacional.html';
			break;
		case 15:
			require_once  'templates/cabecera_form.html';
			require_once  'templates/form_archivos/form_archivos_alta_extranjero.html';
			break;
		case 16:
			require_once  'templates/cabecera.html';
			require_once  'templates/form_end.html';
			break;
		default:
			break;
	}

	require_once  'templates/pie_pagina.html';
	?>
	</div>
</body>

</html>