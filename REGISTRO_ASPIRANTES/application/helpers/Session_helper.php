<?php
function is_logged_in()
{
    // Get current CodeIgniter instance
    $CI = &get_instance();
    // We need to use $CI->session instead of $this->session
    $user = $CI->session->userdata("logged_in");

    if (isset($user) && $user === true) {
        return true;
    } else {
        return false;
    }
}
function is_registrado_in()
{
    // Get current CodeIgniter instance
    $CI = &get_instance();
    // We need to use $CI->session instead of $this->session
    $registro = $CI->session->userdata("registro_id");
    $aspirante = $CI->session->userdata("aspirante_id");
    if (isset($aspirante) && isset($registro)) {
        return true;
    } else {
        return false;
    }
}
function is_aspirante_same($id)
{
    // Get current CodeIgniter instance
    $CI = &get_instance();
    // We need to use $CI->session instead of $this->session
    $registro = $CI->session->userdata("aspirante_id");
    if ($registro == $id) {
        return true;
    } else {
        return false;
    }
}
function is_datos_aspirante_same($id)
{
    // Get current CodeIgniter instance
    $CI = &get_instance();
    // We need to use $CI->session instead of $this->session
    $registro = $CI->session->userdata("datos_aspirante_id");
    if ($registro == $id) {
        return true;
    } else {
        return false;
    }
}
function is_datos_aspirante_id_in()
{
    // Get current CodeIgniter instance
    $CI = &get_instance();
    // We need to use $CI->session instead of $this->session
    $datos_aspirante_id  = $CI->session->userdata("datos_aspirante_id");
    if (isset($datos_aspirante_id)) {
        return true;
    } else {
        return false;
    }
}

function is_registro_end()
{
    // Get current CodeIgniter instance
    $CI = &get_instance();
    // We need to use $CI->session instead of $this->session
    $user = $CI->session->userdata("registro_end");

    if (isset($user) && $user === true) {
        return true;
    } else {
        return false;
    }
}