<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Entidades extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('entidades_model');
    }
    public function get_all_entidades()
    {
        $entidades =  $this->entidades_model->get_all_entidades();
        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode(['entidades' => $entidades], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
            ->_display();
        exit;
    }
}
