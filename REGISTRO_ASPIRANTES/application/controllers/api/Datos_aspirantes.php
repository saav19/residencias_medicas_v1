<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Datos_aspirantes extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
       require_once APPPATH.'helpers/Sesiones_helper.php';
        $this->load->model('datos_aspirantes_model');
        $this->load->library('session');
        $_POST = json_decode(file_get_contents('php://input'), true);
    }
    //------ get's by id´s
    public function get_datos_aspirante_by_id()
    {
        $this->_validate_session();
        $datos_aspirante_id = $this->_get_datos_aspirante_id_by_registro_id();
        if ($datos_aspirante_id === NULL) {
            $this->_response(['code' => 404, 'data' => []]);
        }
        $datos_aspirante = $this->datos_aspirantes_model->get_datos_aspirante_by_id($datos_aspirante_id['datos_aspirante_id']);
        if ($datos_aspirante === NULL) {
            $this->_response(['code' => 404, 'data' => []]);
        }
        $this->session->datos_aspirante_id = $datos_aspirante_id['datos_aspirante_id'];
        $this->_response(['code' => 200, 'data' => ['datos_aspirante' => $datos_aspirante]]);
    }

    //---- inserts and updates

    //----- alta especialidad
    public function alta_especialidad_nacional_post()
    {
        $this->_validate_session();
        $this->_form_validation_alta_especialidad_nacional();
        $this->db->trans_start();
        if (!$this->_exists_datos_aspirante_id()) {
            $datos_aspirante_id = $this->datos_aspirantes_model->insert($this->_fill_data_alta_especialidad_nacional(false));
        } else {
            $this->_validate_same_datos_aspirante_id();
            $datos_aspirante_id = $this->datos_aspirantes_model->update($this->input->post('id'), $this->_fill_data_alta_especialidad_nacional(true));
        }
        if (!$datos_aspirante_id) {
            $this->_response(['code' => 500, 'data' => ['mensaje' => "No se logro insertar los datos."]]);
        }
        if (!$this->_update_registro($datos_aspirante_id)) {
            $this->_response(['code' => 500, 'data' => ['mensaje' => "No se logro actualizar los datos."]]);
        }
        $this->db->trans_complete();
        $this->_response(['code' => 200, 'data' => ['mensaje' => 'Se guardaron los datos.']]);
    }

    public function alta_especialidad_extranjero_post()
    {
        $this->_validate_session();
        $this->_form_validation_alta_especialidad_extranjero();
        $this->db->trans_start();
        if (!$this->_exists_datos_aspirante_id()) {
            $datos_aspirante_id = $this->datos_aspirantes_model->insert($this->_fill_data_alta_especialidad_extranjero(false));
        } else {
            $this->_validate_same_datos_aspirante_id();
            $datos_aspirante_id = $this->datos_aspirantes_model->update($this->input->post('id'), $this->_fill_data_alta_especialidad_extranjero(true));
        }
        if (!$datos_aspirante_id) {
            $this->_response(['code' => 500, 'data' => ['mensaje' => "No se logro insertar los datos."]]);
        }
        if (!$this->_update_registro($datos_aspirante_id)) {
            $this->_response(['code' => 500, 'data' => ['mensaje' => "No se logro actualizar los datos."]]);
        }
        $this->db->trans_complete();
        $this->_response(['code' => 200, 'data' => ['mensaje' => 'Se guardaron los datos.']]);
    }

    //----- directa
    public function directa_nacional_post()
    {
        $this->_validate_session();
        $this->_form_validation_directa_nacional();
        $this->db->trans_start();
        if (!$this->_exists_datos_aspirante_id()) {
            $datos_aspirante_id = $this->datos_aspirantes_model->insert($this->_fill_data_directa_nacional(false));
        } else {
            $this->_validate_same_datos_aspirante_id();
            $datos_aspirante_id = $this->datos_aspirantes_model->update($this->input->post('id'), $this->_fill_data_directa_nacional(true));
        }
        if (!$datos_aspirante_id) {
            $this->_response(['code' => 500, 'data' => ['mensaje' => "No se logro insertar los datos."]]);
        }
        if (!$this->_update_registro($datos_aspirante_id)) {
            $this->_response(['code' => 500, 'data' => ['mensaje' => "No se logro actualizar los datos."]]);
        }
        $this->db->trans_complete();
        $this->_response(['code' => 200, 'data' => ['mensaje' => 'Se guardaron los datos.']]);
    }

    public function directa_extranjero_post()
    {
        $this->_validate_session();
        $this->_form_validation_directa_extranjero();
        $this->db->trans_start();
        if (!$this->_exists_datos_aspirante_id()) {
            $datos_aspirante_id = $this->datos_aspirantes_model->insert($this->_fill_data_directa_extranjero(false));
        } else {
            $this->_validate_same_datos_aspirante_id();
            $datos_aspirante_id = $this->datos_aspirantes_model->update($this->input->post('id'), $this->_fill_data_directa_extranjero(true));
        }
        if (!$datos_aspirante_id) {
            $this->_response(['code' => 500, 'data' => ['mensaje' => "No se logro insertar los datos."]]);
        }
        if (!$this->_update_registro($datos_aspirante_id)) {
            $this->_response(['code' => 500, 'data' => ['mensaje' => "No se logro actualizar los datos."]]);
        }
        $this->db->trans_complete();
        $this->_response(['code' => 200, 'data' => ['mensaje' => 'Se guardaron los datos.']]);
    }


    //---- indirecta
    public function indirecta_nacional_post()
    {
        $this->_validate_session();
        $this->_form_validation_indirecta_nacional();
        $this->db->trans_start();
        if (!$this->_exists_datos_aspirante_id()) {
            $datos_aspirante_id = $this->datos_aspirantes_model->insert($this->_fill_data_indirecta_nacional(false));
        } else {
            $this->_validate_same_datos_aspirante_id();
            $datos_aspirante_id = $this->datos_aspirantes_model->update($this->input->post('id'), $this->_fill_data_indirecta_nacional(true));
        }
        if (!$datos_aspirante_id) {
            $this->_response(['code' => 500, 'data' => ['mensaje' => "No se logro insertar los datos."]]);
        }
        if (!$this->_update_registro($datos_aspirante_id)) {
            $this->_response(['code' => 500, 'data' => ['mensaje' => "No se logro actualizar los datos."]]);
        }
        $this->db->trans_complete();
        $this->_response(['code' => 200, 'data' => ['mensaje' => 'Se guardaron los datos.']]);
    }

    public function indirecta_extranjero_post()
    {
        $this->_validate_session();
        $this->_form_validation_indirecta_extranjero();
        $this->db->trans_start();
        if (!$this->_exists_datos_aspirante_id()) {
            $datos_aspirante_id = $this->datos_aspirantes_model->insert($this->_fill_data_indirecta_extranjero(false));
        } else {
            $this->_validate_same_datos_aspirante_id();
            $datos_aspirante_id = $this->datos_aspirantes_model->update($this->input->post('id'), $this->_fill_data_indirecta_extranjero(true));
        }
        if (!$datos_aspirante_id) {
            $this->_response(['code' => 500, 'data' => ['mensaje' => "No se logro insertar los datos."]]);
        }
        if (!$this->_update_registro($datos_aspirante_id)) {
            $this->_response(['code' => 500, 'data' => ['mensaje' => "No se logro actualizar los datos."]]);
        }
        $this->db->trans_complete();
        $this->_response(['code' => 200, 'data' => ['mensaje' => 'Se guardaron los datos.']]);
    }


    //---- funciones publicas
    public function year_valido($year_egreso)
    {
        $year_actual = date("Y");
        if ($year_egreso <= $year_actual) {
            return true;
        } else {
            $this->form_validation->set_message('year_valido', 'El  campo {field} es mayor que el año actual');
            return FALSE;
        }
    }
    public function promedio_valido($promedio)
    {
        if (!is_numeric($promedio)) {
            if (!ctype_alpha($promedio)) {
                $this->form_validation->set_message('promedio_valido', 'El  campo {field} no es una calificacion valida');
                return FALSE;
            }
        } else {
            if ($promedio > 10) {
                $this->form_validation->set_message('promedio_valido', 'El  campo {field} no puede ser mayor de 10');
                return FALSE;
            }
        }
        return true;
    }
    //----- funciones privadas
    private function _get_datos_aspirante_id_by_registro_id()
    {
        $this->load->model('registros_model');
        return $this->registros_model->get_datos_aspirante_id_by_registro_id($this->session->registro_id);
    }

    private function _update_registro($datos_aspirante_id)
    {
        $data['datos_aspirante_id'] = $datos_aspirante_id;
        $this->load->model('registros_model');
        if ($this->registros_model->update($this->session->registro_id, $data)) {
            return true;
        } else {
            return false;
        }
    }

    private function _response($response)
    {
        $this->output
            ->set_status_header($response['code'])
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response['data'], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
            ->_display();
        exit;
    }

    //---------------------------fill data de alta especialidad
    private function _fill_data_alta_especialidad_nacional($bandera)
    {
        $data_insert['codigo_postal_id'] = $this->input->post('codigo_postal_id');
        $data_insert['domicilio_calle'] = strtoupper($this->input->post('domicilio_calle'));
        $data_insert['domicilio_numero_exterior'] = $this->input->post('domicilio_numero_exterior');
        $data_insert['domicilio_numero_interior'] = (isset($_POST["domicilio_numero_interior"]) && $_POST["domicilio_numero_interior"] !== '') ? $_POST["domicilio_numero_interior"] : NULL;
        $data_insert['domicilio_entre_calle_1'] = strtoupper($this->input->post('domicilio_entre_calle_1'));
        $data_insert['domicilio_entre_calle_2'] = strtoupper($this->input->post('domicilio_entre_calle_2'));
        $data_insert['correo_electronico'] =  $this->input->post('correo_electronico');
        $data_insert['estado_civil'] = $this->input->post('estado_civil');
        $data_insert['telefono_fijo'] = (isset($_POST["telefono_fijo"]) && $_POST["telefono_fijo"] !== '') ? $this->input->post('telefono_fijo') : NULL;
        $data_insert['telefono_movil'] = $this->input->post('telefono_movil');
        $data_insert['grupo_sanguineo'] = $this->input->post('grupo_sanguineo');
        $data_insert['factor_rh'] = $this->input->post('factor_rh');
        $data_insert['talla_bata'] = $this->input->post('talla_bata');
        $data_insert['talla_uniforme_quirurgico'] = $this->input->post('talla_uniforme_quirurgico');
        $data_insert['talla_pantalon'] = $this->input->post('talla_pantalon');
        $data_insert['talla_zapatos'] = $this->input->post('talla_zapatos');
        $data_insert['folio_enarm'] = (isset($_POST["folio_enarm"]) && $_POST["folio_enarm"] !== '') ? strtoupper($_POST["folio_enarm"]) : NULL;
        $data_insert['cedula_profesional'] = (isset($_POST["cedula_profesional"]) && $_POST["cedula_profesional"] !== '') ? strtoupper($_POST["cedula_profesional"]) : NULL;
        $data_insert['universidad_id'] = $this->input->post('universidad_id');
        $data_insert['hospital_procedencia'] = strtoupper($this->input->post('hospital_procedencia'));
        $data_insert['promedio_licenciatura'] = $this->input->post('promedio_licenciatura');
        $data_insert['ano_egreso_licenciatura'] = $this->input->post('ano_egreso_licenciatura');
        $data_insert['universidad_otra_opcion'] = (isset($_POST["universidad_otra_opcion"]) && $_POST["universidad_otra_opcion"] !== '') ? strtoupper($_POST["universidad_otra_opcion"]) : NULL;
        $data_insert['institucion_entidad_id'] = $this->input->post('institucion_entidad_id');
        $data_insert['status'] = $this->input->post('status');
        if (!$bandera) {
            $data_insert['created_at'] = date("Y-m-d H:i:s");
        } else {
            $data_insert['updated_at'] = date("Y-m-d H:i:s");
        }
        return $data_insert;
    }
    private function _fill_data_alta_especialidad_extranjero($bandera)
    {
        $data_insert['domicilio_extranjero'] = strtoupper($this->input->post('domicilio_extranjero'));
        $data_insert['correo_electronico'] =  $this->input->post('correo_electronico');
        $data_insert['estado_civil'] = $this->input->post('estado_civil');
        $data_insert['telefono_fijo'] = (isset($_POST["telefono_fijo"]) && $_POST["telefono_fijo"] !== '') ? $this->input->post('telefono_fijo') : NULL;
        $data_insert['telefono_movil'] = $this->input->post('telefono_movil');
        $data_insert['grupo_sanguineo'] = $this->input->post('grupo_sanguineo');
        $data_insert['factor_rh'] = $this->input->post('factor_rh');
        $data_insert['talla_bata'] = $this->input->post('talla_bata');
        $data_insert['talla_uniforme_quirurgico'] = $this->input->post('talla_uniforme_quirurgico');
        $data_insert['talla_pantalon'] = $this->input->post('talla_pantalon');
        $data_insert['talla_zapatos'] = $this->input->post('talla_zapatos');
        $data_insert['folio_enarm'] = (isset($_POST["folio_enarm"]) && $_POST["folio_enarm"] !== '') ? strtoupper($_POST["folio_enarm"]) : NULL;
        $data_insert['cedula_profesional'] = (isset($_POST["cedula_profesional"]) && $_POST["cedula_profesional"] !== '') ? strtoupper($_POST["cedula_profesional"]) : NULL;
        $data_insert['universidad_id'] = $this->input->post('universidad_id');
        $data_insert['hospital_procedencia'] = strtoupper($this->input->post('hospital_procedencia'));
        $data_insert['promedio_licenciatura'] = $this->input->post('promedio_licenciatura');
        $data_insert['ano_egreso_licenciatura'] = $this->input->post('ano_egreso_licenciatura');
        $data_insert['universidad_otra_opcion'] = (isset($_POST["universidad_otra_opcion"]) && $_POST["universidad_otra_opcion"] !== '') ? strtoupper($_POST["universidad_otra_opcion"]) : NULL;
        $data_insert['institucion_entidad_id'] = $this->input->post('institucion_entidad_id');
        $data_insert['status'] = $this->input->post('status');
        if (!$bandera) {
            $data_insert['created_at'] = date("Y-m-d H:i:s");
        } else {
            $data_insert['updated_at'] = date("Y-m-d H:i:s");
        }
        return $data_insert;
    }

    //---------------------------fill data de directa
    private function _fill_data_directa_nacional($bandera)
    {
        $data_insert['codigo_postal_id'] = $this->input->post('codigo_postal_id');
        $data_insert['domicilio_calle'] = strtoupper($this->input->post('domicilio_calle'));
        $data_insert['domicilio_numero_exterior'] = $this->input->post('domicilio_numero_exterior');
        $data_insert['domicilio_numero_interior'] = (isset($_POST["domicilio_numero_interior"]) && $_POST["domicilio_numero_interior"] !== '') ? $_POST["domicilio_numero_interior"] : NULL;
        $data_insert['domicilio_entre_calle_1'] = strtoupper($this->input->post('domicilio_entre_calle_1'));
        $data_insert['domicilio_entre_calle_2'] = strtoupper($this->input->post('domicilio_entre_calle_2'));
        $data_insert['correo_electronico'] =  $this->input->post('correo_electronico');
        $data_insert['estado_civil'] = $this->input->post('estado_civil');
        $data_insert['telefono_fijo'] = (isset($_POST["telefono_fijo"]) && $_POST["telefono_fijo"] !== '') ? $this->input->post('telefono_fijo') : NULL;
        $data_insert['telefono_movil'] = $this->input->post('telefono_movil');
        $data_insert['grupo_sanguineo'] = $this->input->post('grupo_sanguineo');
        $data_insert['factor_rh'] = $this->input->post('factor_rh');
        $data_insert['talla_bata'] = $this->input->post('talla_bata');
        $data_insert['talla_uniforme_quirurgico'] = $this->input->post('talla_uniforme_quirurgico');
        $data_insert['talla_pantalon'] = $this->input->post('talla_pantalon');
        $data_insert['talla_zapatos'] = $this->input->post('talla_zapatos');
        $data_insert['folio_enarm'] = (isset($_POST["folio_enarm"]) && $_POST["folio_enarm"] !== '') ? strtoupper($_POST["folio_enarm"]) : NULL;
        $data_insert['cedula_profesional'] = (isset($_POST["cedula_profesional"]) && $_POST["cedula_profesional"] !== '') ? strtoupper($_POST["cedula_profesional"]) : NULL;
        $data_insert['promedio_licenciatura'] = $this->input->post('promedio_licenciatura');
        $data_insert['ano_egreso_licenciatura'] = $this->input->post('ano_egreso_licenciatura');
        $data_insert['status'] = $this->input->post('status');
        if (!$bandera) {
            $data_insert['created_at'] = date("Y-m-d H:i:s");
        } else {
            $data_insert['updated_at'] = date("Y-m-d H:i:s");
        }
        return $data_insert;
    }
    private function _fill_data_directa_extranjero($bandera)
    {
        $data_insert['domicilio_extranjero'] = strtoupper($this->input->post('domicilio_extranjero'));
        $data_insert['correo_electronico'] =  $this->input->post('correo_electronico');
        $data_insert['estado_civil'] = $this->input->post('estado_civil');
        $data_insert['telefono_fijo'] = (isset($_POST["telefono_fijo"]) && $_POST["telefono_fijo"] !== '') ? $this->input->post('telefono_fijo') : NULL;
        $data_insert['telefono_movil'] = $this->input->post('telefono_movil');
        $data_insert['grupo_sanguineo'] = $this->input->post('grupo_sanguineo');
        $data_insert['factor_rh'] = $this->input->post('factor_rh');
        $data_insert['talla_bata'] = $this->input->post('talla_bata');
        $data_insert['talla_uniforme_quirurgico'] = $this->input->post('talla_uniforme_quirurgico');
        $data_insert['talla_pantalon'] = $this->input->post('talla_pantalon');
        $data_insert['talla_zapatos'] = $this->input->post('talla_zapatos');
        $data_insert['folio_enarm'] = (isset($_POST["folio_enarm"]) && $_POST["folio_enarm"] !== '') ? strtoupper($_POST["folio_enarm"]) : NULL;
        $data_insert['cedula_profesional'] = (isset($_POST["cedula_profesional"]) && $_POST["cedula_profesional"] !== '') ? strtoupper($_POST["cedula_profesional"]) : NULL;
        $data_insert['promedio_licenciatura'] = $this->input->post('promedio_licenciatura');
        $data_insert['ano_egreso_licenciatura'] = $this->input->post('ano_egreso_licenciatura');
        $data_insert['status'] = $this->input->post('status');
        if (!$bandera) {
            $data_insert['created_at'] = date("Y-m-d H:i:s");
        } else {
            $data_insert['updated_at'] = date("Y-m-d H:i:s");
        }
        return $data_insert;
    }

    //---------------------------fill data de indirecta
    private function _fill_data_indirecta_nacional($bandera)
    {
        $data_insert['codigo_postal_id'] = $this->input->post('codigo_postal_id');
        $data_insert['domicilio_calle'] = strtoupper($this->input->post('domicilio_calle'));
        $data_insert['domicilio_numero_exterior'] = $this->input->post('domicilio_numero_exterior');
        $data_insert['domicilio_numero_interior'] = (isset($_POST["domicilio_numero_interior"]) && $_POST["domicilio_numero_interior"] !== '') ? $_POST["domicilio_numero_interior"] : NULL;
        $data_insert['domicilio_entre_calle_1'] = strtoupper($this->input->post('domicilio_entre_calle_1'));
        $data_insert['domicilio_entre_calle_2'] = strtoupper($this->input->post('domicilio_entre_calle_2'));
        $data_insert['correo_electronico'] =  $this->input->post('correo_electronico');
        $data_insert['estado_civil'] = $this->input->post('estado_civil');
        $data_insert['telefono_fijo'] = (isset($_POST["telefono_fijo"]) && $_POST["telefono_fijo"] !== '') ? $this->input->post('telefono_fijo') : NULL;
        $data_insert['telefono_movil'] = $this->input->post('telefono_movil');
        $data_insert['grupo_sanguineo'] = $this->input->post('grupo_sanguineo');
        $data_insert['factor_rh'] = $this->input->post('factor_rh');
        $data_insert['talla_bata'] = $this->input->post('talla_bata');
        $data_insert['talla_uniforme_quirurgico'] = $this->input->post('talla_uniforme_quirurgico');
        $data_insert['talla_pantalon'] = $this->input->post('talla_pantalon');
        $data_insert['talla_zapatos'] = $this->input->post('talla_zapatos');
        $data_insert['folio_enarm'] = (isset($_POST["folio_enarm"]) && $_POST["folio_enarm"] !== '') ? strtoupper($_POST["folio_enarm"]) : NULL;
        $data_insert['cedula_profesional'] = (isset($_POST["cedula_profesional"]) && $_POST["cedula_profesional"] !== '') ? strtoupper($_POST["cedula_profesional"]) : NULL;
        $data_insert['universidad_id'] = $this->input->post('universidad_id');
        $data_insert['hospital_procedencia'] = strtoupper($this->input->post('hospital_procedencia'));
        $data_insert['promedio_licenciatura'] = $this->input->post('promedio_licenciatura');
        $data_insert['ano_egreso_licenciatura'] = $this->input->post('ano_egreso_licenciatura');
        $data_insert['universidad_otra_opcion'] = (isset($_POST["universidad_otra_opcion"]) && $_POST["universidad_otra_opcion"] !== '') ? strtoupper($_POST["universidad_otra_opcion"]) : NULL;
        $data_insert['institucion_entidad_id'] = $this->input->post('institucion_entidad_id');
        $data_insert['status'] = $this->input->post('status');
        if (!$bandera) {
            $data_insert['created_at'] = date("Y-m-d H:i:s");
        } else {
            $data_insert['updated_at'] = date("Y-m-d H:i:s");
        }
        return $data_insert;
    }
    private function _fill_data_indirecta_extranjero($bandera)
    {
        $data_insert['domicilio_extranjero'] = strtoupper($this->input->post('domicilio_extranjero'));
        $data_insert['correo_electronico'] =  $this->input->post('correo_electronico');
        $data_insert['estado_civil'] = $this->input->post('estado_civil');
        $data_insert['telefono_fijo'] = (isset($_POST["telefono_fijo"]) && $_POST["telefono_fijo"] !== '') ? $this->input->post('telefono_fijo') : NULL;
        $data_insert['telefono_movil'] = $this->input->post('telefono_movil');
        $data_insert['grupo_sanguineo'] = $this->input->post('grupo_sanguineo');
        $data_insert['factor_rh'] = $this->input->post('factor_rh');
        $data_insert['talla_bata'] = $this->input->post('talla_bata');
        $data_insert['talla_uniforme_quirurgico'] = $this->input->post('talla_uniforme_quirurgico');
        $data_insert['talla_pantalon'] = $this->input->post('talla_pantalon');
        $data_insert['talla_zapatos'] = $this->input->post('talla_zapatos');
        $data_insert['folio_enarm'] = (isset($_POST["folio_enarm"]) && $_POST["folio_enarm"] !== '') ? strtoupper($_POST["folio_enarm"]) : NULL;
        $data_insert['cedula_profesional'] = (isset($_POST["cedula_profesional"]) && $_POST["cedula_profesional"] !== '') ? strtoupper($_POST["cedula_profesional"]) : NULL;
        $data_insert['universidad_id'] = $this->input->post('universidad_id');
        $data_insert['hospital_procedencia'] = strtoupper($this->input->post('hospital_procedencia'));
        $data_insert['promedio_licenciatura'] = $this->input->post('promedio_licenciatura');
        $data_insert['ano_egreso_licenciatura'] = $this->input->post('ano_egreso_licenciatura');
        $data_insert['universidad_otra_opcion'] = (isset($_POST["universidad_otra_opcion"]) && $_POST["universidad_otra_opcion"] !== '') ? strtoupper($_POST["universidad_otra_opcion"]) : NULL;
        $data_insert['institucion_entidad_id'] = $this->input->post('institucion_entidad_id');
        $data_insert['status'] = $this->input->post('status');
        if (!$bandera) {
            $data_insert['created_at'] = date("Y-m-d H:i:s");
        } else {
            $data_insert['updated_at'] = date("Y-m-d H:i:s");
        }
        return $data_insert;
    }

    //------validaciones 

    //------validacion_alta
    private function _form_validation_alta_especialidad_nacional()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('codigo_postal_id', 'Codigo Postal', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('institucion_entidad_id', 'Entidad De La Institucion', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('universidad_id', 'Univesidad', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('correo_electronico', 'Correo Electronico', 'trim|required|valid_email');
        $this->form_validation->set_rules('comfirm_correo_electronico', 'Comfirmar correo Electronico', 'trim|required|valid_email|matches[correo_electronico]');
        $this->form_validation->set_rules('estado_civil', 'Estado Civil', 'trim|required|is_natural_no_zero|in_list[1,2,3,4,5]');
        $this->form_validation->set_rules('domicilio_calle', 'Calle', 'trim|required|max_length[200]');
        $this->form_validation->set_rules('domicilio_numero_exterior', 'Número Exterior', 'trim|required|max_length[20]');
        $this->form_validation->set_rules('domicilio_numero_interior', 'Número Interior', 'trim|max_length[20]');
        $this->form_validation->set_rules('domicilio_entre_calle_1', 'Entre Calle 1', 'trim|required|max_length[200]');
        $this->form_validation->set_rules('domicilio_entre_calle_2', 'Entre  Calle 2', 'trim|required|max_length[200]');
        $this->form_validation->set_rules('telefono_fijo', 'Telefono Fijo', 'trim|numeric|min_length[10]|max_length[15]');
        $this->form_validation->set_rules('telefono_movil', 'Telefono Celular', 'trim|required|numeric|min_length[10]|max_length[15]');
        $this->form_validation->set_rules('grupo_sanguineo', 'Grupo Sanguine', 'trim|required|is_natural_no_zero|in_list[1,2,3,4]');
        $this->form_validation->set_rules('factor_rh', 'Factor RH', 'trim|required|is_natural_no_zero|in_list[1,2]');
        $this->form_validation->set_rules('talla_bata', 'Talla De La Bata', 'trim|required|max_length[2]|in_list[1,2,3,4,5,6,7,8]');
        $this->form_validation->set_rules('talla_uniforme_quirurgico', 'Talla Del Uniforme Quirurgico', 'trim|required|max_length[2]|in_list[1,2,3,4,5]');
        $this->form_validation->set_rules('talla_pantalon', 'Talla Del Pantalon', 'trim|required|max_length[2]|in_list[1,2,3,4,5,6,7,8]');
        $this->form_validation->set_rules('talla_zapatos', 'Talla de Zapatos', 'trim|required|max_length[2]|in_list[1,2,3,4,5,6,7,8]');
        $this->form_validation->set_rules('folio_enarm', 'Folio Del E.N.A.R.M.', 'trim|max_length[30]');
        $this->form_validation->set_rules('cedula_profesional', 'Cedula Profecional', 'trim|required|alpha_numeric|max_length[20]');
        $this->form_validation->set_rules('universidad_otra_opcion', ' Nombre De la Universidad.', 'trim|max_length[150]');
        $this->form_validation->set_rules('hospital_procedencia', 'Hospital de Procedencia', 'trim|required|max_length[150]');
        $this->form_validation->set_rules('ano_egreso_licenciatura', 'Año de Egreso de la Licenciatura', 'trim|required|numeric|max_length[4]|callback_year_valido');
        $this->form_validation->set_rules('promedio_licenciatura', 'Calificacion Obtenida', 'trim|required|max_length[5]|callback_promedio_valido');
        if (!$this->form_validation->run()) {
            $this->_response(['code' => 400, 'data' => ['mensaje' => validation_errors()]]);
        }
    }
    private function _form_validation_alta_especialidad_extranjero()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('institucion_entidad_id', 'Entidad De La Institucion', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('universidad_id', 'Univesidad', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('correo_electronico', 'Correo Electronico', 'trim|required|valid_email');
        $this->form_validation->set_rules('comfirm_correo_electronico', 'Comfirmar correo Electronico', 'trim|required|valid_email|matches[correo_electronico]');
        $this->form_validation->set_rules('estado_civil', 'Estado Civil', 'trim|required|is_natural_no_zero|in_list[1,2,3,4,5]');
        $this->form_validation->set_rules('domicilio_extranjero', 'Dirección', 'trim|required|max_length[400]');
        $this->form_validation->set_rules('telefono_fijo', 'Telefono Fijo', 'trim|numeric|min_length[10]|max_length[15]');
        $this->form_validation->set_rules('telefono_movil', 'Telefono Celular', 'trim|required|numeric|min_length[10]|max_length[15]');
        $this->form_validation->set_rules('grupo_sanguineo', 'Grupo Sanguine', 'trim|required|is_natural_no_zero|in_list[1,2,3,4]');
        $this->form_validation->set_rules('factor_rh', 'Factor RH', 'trim|required|is_natural_no_zero|in_list[1,2]');
        $this->form_validation->set_rules('talla_bata', 'Talla De La Bata', 'trim|required|max_length[2]|in_list[1,2,3,4,5,6,7,8]');
        $this->form_validation->set_rules('talla_uniforme_quirurgico', 'Talla Del Uniforme Quirurgico', 'trim|required|max_length[2]|in_list[1,2,3,4,5]');
        $this->form_validation->set_rules('talla_pantalon', 'Talla Del Pantalon', 'trim|required|max_length[2]|in_list[1,2,3,4,5,6,7,8]');
        $this->form_validation->set_rules('talla_zapatos', 'Talla de Zapatos', 'trim|required|max_length[2]|in_list[1,2,3,4,5,6,7,8]');
        $this->form_validation->set_rules('folio_enarm', 'Folio Del E.N.A.R.M.', 'trim|max_length[30]');
        $this->form_validation->set_rules('cedula_profesional', 'Cedula Profecional', 'trim|required|alpha_numeric|max_length[20]');
        $this->form_validation->set_rules('universidad_otra_opcion', ' Nombre De la Universidad.', 'trim|max_length[150]');
        $this->form_validation->set_rules('hospital_procedencia', 'Hospital de Procedencia', 'trim|required|max_length[150]');
        $this->form_validation->set_rules('ano_egreso_licenciatura', 'Año de Egreso de la Licenciatura', 'trim|required|numeric|max_length[4]|callback_year_valido');
        $this->form_validation->set_rules('promedio_licenciatura', 'Calificacion Obtenida', 'trim|required|max_length[5]|callback_promedio_valido');
        if (!$this->form_validation->run()) {
            $this->_response(['code' => 400, 'data' => ['mensaje' => validation_errors()]]);
        }
    }

    //------validacion_directa
    private function _form_validation_directa_nacional()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('codigo_postal_id', 'Codigo Postal', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('correo_electronico', 'Correo Electronico', 'trim|required|valid_email');
        $this->form_validation->set_rules('comfirm_correo_electronico', 'Comfirmar correo Electronico', 'trim|required|valid_email|matches[correo_electronico]');
        $this->form_validation->set_rules('estado_civil', 'Estado Civil', 'trim|required|is_natural_no_zero|in_list[1,2,3,4,5]');
        $this->form_validation->set_rules('domicilio_calle', 'Calle', 'trim|required|max_length[200]');
        $this->form_validation->set_rules('domicilio_numero_exterior', 'Número Exterior', 'trim|required|max_length[20]');
        $this->form_validation->set_rules('domicilio_numero_interior', 'Número Interior', 'trim|max_length[20]');
        $this->form_validation->set_rules('domicilio_entre_calle_1', 'Entre Calle 1', 'trim|required|max_length[200]');
        $this->form_validation->set_rules('domicilio_entre_calle_2', 'Entre  Calle 2', 'trim|required|max_length[200]');
        $this->form_validation->set_rules('telefono_fijo', 'Telefono Fijo', 'trim|numeric|min_length[10]|max_length[15]');
        $this->form_validation->set_rules('telefono_movil', 'Telefono Celular', 'trim|required|numeric|min_length[10]|max_length[15]');
        $this->form_validation->set_rules('grupo_sanguineo', 'Grupo Sanguine', 'trim|required|is_natural_no_zero|in_list[1,2,3,4]');
        $this->form_validation->set_rules('factor_rh', 'Factor RH', 'trim|required|is_natural_no_zero|in_list[1,2]');
        $this->form_validation->set_rules('talla_bata', 'Talla De La Bata', 'trim|required|max_length[2]|in_list[1,2,3,4,5,6,7,8]');
        $this->form_validation->set_rules('talla_uniforme_quirurgico', 'Talla Del Uniforme Quirurgico', 'trim|required|max_length[2]|in_list[1,2,3,4,5]');
        $this->form_validation->set_rules('talla_pantalon', 'Talla Del Pantalon', 'trim|required|max_length[2]|in_list[1,2,3,4,5,6,7,8]');
        $this->form_validation->set_rules('talla_zapatos', 'Talla de Zapatos', 'trim|required|max_length[2]|in_list[1,2,3,4,5,6,7,8]');
        $this->form_validation->set_rules('folio_enarm', 'Folio Del E.N.A.R.M.', 'trim|max_length[30]');
        $this->form_validation->set_rules('cedula_profesional', 'Cedula Profecional', 'trim|alpha_numeric|max_length[20]');
        $this->form_validation->set_rules('ano_egreso_licenciatura', 'Año de Egreso de la Licenciatura', 'trim|required|numeric|max_length[4]|callback_year_valido');
        $this->form_validation->set_rules('promedio_licenciatura', 'Calificacion Obtenida', 'trim|required|max_length[5]|callback_promedio_valido');
        if (!$this->form_validation->run()) {
            $this->_response(['code' => 400, 'data' => ['mensaje' => validation_errors()]]);
        }
    }
    private function _form_validation_directa_extranjero()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('correo_electronico', 'Correo Electronico', 'trim|required|valid_email');
        $this->form_validation->set_rules('comfirm_correo_electronico', 'Comfirmar correo Electronico', 'trim|required|valid_email|matches[correo_electronico]');
        $this->form_validation->set_rules('estado_civil', 'Estado Civil', 'trim|required|is_natural_no_zero|in_list[1,2,3,4,5]');
        $this->form_validation->set_rules('domicilio_extranjero', 'Dirección', 'trim|required|max_length[400]');
        $this->form_validation->set_rules('telefono_fijo', 'Telefono Fijo', 'trim|numeric|min_length[10]|max_length[15]');
        $this->form_validation->set_rules('telefono_movil', 'Telefono Celular', 'trim|required|numeric|min_length[10]|max_length[15]');
        $this->form_validation->set_rules('grupo_sanguineo', 'Grupo Sanguine', 'trim|required|is_natural_no_zero|in_list[1,2,3,4]');
        $this->form_validation->set_rules('factor_rh', 'Factor RH', 'trim|required|is_natural_no_zero|in_list[1,2]');
        $this->form_validation->set_rules('talla_bata', 'Talla De La Bata', 'trim|required|max_length[2]|in_list[1,2,3,4,5,6,7,8]');
        $this->form_validation->set_rules('talla_uniforme_quirurgico', 'Talla Del Uniforme Quirurgico', 'trim|required|max_length[2]|in_list[1,2,3,4,5]');
        $this->form_validation->set_rules('talla_pantalon', 'Talla Del Pantalon', 'trim|required|max_length[2]|in_list[1,2,3,4,5,6,7,8]');
        $this->form_validation->set_rules('talla_zapatos', 'Talla de Zapatos', 'trim|required|max_length[2]|in_list[1,2,3,4,5,6,7,8]');
        $this->form_validation->set_rules('folio_enarm', 'Folio Del E.N.A.R.M.', 'trim|max_length[30]');
        $this->form_validation->set_rules('cedula_profesional', 'Cedula Profecional', 'trim|alpha_numeric|max_length[20]');
        $this->form_validation->set_rules('ano_egreso_licenciatura', 'Año de Egreso de la Licenciatura', 'trim|required|numeric|max_length[4]|callback_year_valido');
        $this->form_validation->set_rules('promedio_licenciatura', 'Calificacion Obtenida', 'trim|required|max_length[5]|callback_promedio_valido');
        if (!$this->form_validation->run()) {
            $this->_response(['code' => 400, 'data' => ['mensaje' => validation_errors()]]);
        }
    }

    //------validacion_indirecta
    private function _form_validation_indirecta_nacional()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('codigo_postal_id', 'Codigo Postal', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('institucion_entidad_id', 'Entidad De La Institucion', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('universidad_id', 'Univesidad', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('correo_electronico', 'Correo Electronico', 'trim|required|valid_email');
        $this->form_validation->set_rules('comfirm_correo_electronico', 'Comfirmar correo Electronico', 'trim|required|valid_email|matches[correo_electronico]');
        $this->form_validation->set_rules('estado_civil', 'Estado Civil', 'trim|required|is_natural_no_zero|in_list[1,2,3,4,5]');
        $this->form_validation->set_rules('domicilio_calle', 'Calle', 'trim|required|max_length[200]');
        $this->form_validation->set_rules('domicilio_numero_exterior', 'Número Exterior', 'trim|required|max_length[20]');
        $this->form_validation->set_rules('domicilio_numero_interior', 'Número Interior', 'trim|max_length[20]');
        $this->form_validation->set_rules('domicilio_entre_calle_1', 'Entre Calle 1', 'trim|required|max_length[200]');
        $this->form_validation->set_rules('domicilio_entre_calle_2', 'Entre  Calle 2', 'trim|required|max_length[200]');
        $this->form_validation->set_rules('telefono_fijo', 'Telefono Fijo', 'trim|numeric|min_length[10]|max_length[15]');
        $this->form_validation->set_rules('telefono_movil', 'Telefono Celular', 'trim|required|numeric|min_length[10]|max_length[15]');
        $this->form_validation->set_rules('grupo_sanguineo', 'Grupo Sanguine', 'trim|required|is_natural_no_zero|in_list[1,2,3,4]');
        $this->form_validation->set_rules('factor_rh', 'Factor RH', 'trim|required|is_natural_no_zero|in_list[1,2]');
        $this->form_validation->set_rules('talla_bata', 'Talla De La Bata', 'trim|required|max_length[2]|in_list[1,2,3,4,5,6,7,8]');
        $this->form_validation->set_rules('talla_uniforme_quirurgico', 'Talla Del Uniforme Quirurgico', 'trim|required|max_length[2]|in_list[1,2,3,4,5]');
        $this->form_validation->set_rules('talla_pantalon', 'Talla Del Pantalon', 'trim|required|max_length[2]|in_list[1,2,3,4,5,6,7,8]');
        $this->form_validation->set_rules('talla_zapatos', 'Talla de Zapatos', 'trim|required|max_length[2]|in_list[1,2,3,4,5,6,7,8]');
        $this->form_validation->set_rules('folio_enarm', 'Folio Del E.N.A.R.M.', 'trim|max_length[30]');
        $this->form_validation->set_rules('cedula_profesional', 'Cedula Profecional', 'trim|required|alpha_numeric|max_length[20]');
        $this->form_validation->set_rules('universidad_otra_opcion', ' Nombre De la Universidad.', 'trim|max_length[150]');
        $this->form_validation->set_rules('hospital_procedencia', 'Hospital de Procedencia', 'trim|required|max_length[150]');
        $this->form_validation->set_rules('ano_egreso_licenciatura', 'Año de Egreso de la Licenciatura', 'trim|required|numeric|max_length[4]|callback_year_valido');
        $this->form_validation->set_rules('promedio_licenciatura', 'Calificacion Obtenida', 'trim|required|max_length[5]|callback_promedio_valido');
        if (!$this->form_validation->run()) {
            $this->_response(['code' => 400, 'data' => ['mensaje' => validation_errors()]]);
        }
    }
    private function _form_validation_indirecta_extranjero()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('institucion_entidad_id', 'Entidad De La Institucion', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('universidad_id', 'Univesidad', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('correo_electronico', 'Correo Electronico', 'trim|required|valid_email');
        $this->form_validation->set_rules('comfirm_correo_electronico', 'Comfirmar correo Electronico', 'trim|required|valid_email|matches[correo_electronico]');
        $this->form_validation->set_rules('estado_civil', 'Estado Civil', 'trim|required|is_natural_no_zero|in_list[1,2,3,4,5]');
        $this->form_validation->set_rules('domicilio_extranjero', 'Dirección', 'trim|required|max_length[400]');
        $this->form_validation->set_rules('telefono_fijo', 'Telefono Fijo', 'trim|numeric|min_length[10]|max_length[15]');
        $this->form_validation->set_rules('telefono_movil', 'Telefono Celular', 'trim|required|numeric|min_length[10]|max_length[15]');
        $this->form_validation->set_rules('grupo_sanguineo', 'Grupo Sanguine', 'trim|required|is_natural_no_zero|in_list[1,2,3,4]');
        $this->form_validation->set_rules('factor_rh', 'Factor RH', 'trim|required|is_natural_no_zero|in_list[1,2]');
        $this->form_validation->set_rules('talla_bata', 'Talla De La Bata', 'trim|required|max_length[2]|in_list[1,2,3,4,5,6,7,8]');
        $this->form_validation->set_rules('talla_uniforme_quirurgico', 'Talla Del Uniforme Quirurgico', 'trim|required|max_length[2]|in_list[1,2,3,4,5]');
        $this->form_validation->set_rules('talla_pantalon', 'Talla Del Pantalon', 'trim|required|max_length[2]|in_list[1,2,3,4,5,6,7,8]');
        $this->form_validation->set_rules('talla_zapatos', 'Talla de Zapatos', 'trim|required|max_length[2]|in_list[1,2,3,4,5,6,7,8]');
        $this->form_validation->set_rules('folio_enarm', 'Folio Del E.N.A.R.M.', 'trim|max_length[30]');
        $this->form_validation->set_rules('cedula_profesional', 'Cedula Profecional', 'trim|required|alpha_numeric|max_length[20]');
        $this->form_validation->set_rules('universidad_otra_opcion', ' Nombre De la Universidad.', 'trim|max_length[150]');
        $this->form_validation->set_rules('hospital_procedencia', 'Hospital de Procedencia', 'trim|required|max_length[150]');
        $this->form_validation->set_rules('ano_egreso_licenciatura', 'Año de Egreso de la Licenciatura', 'trim|required|numeric|max_length[4]|callback_year_valido');
        $this->form_validation->set_rules('promedio_licenciatura', 'Calificacion Obtenida', 'trim|required|max_length[5]|callback_promedio_valido');
        if (!$this->form_validation->run()) {
            $this->_response(['code' => 400, 'data' => ['mensaje' => validation_errors()]]);
        }
    }

    private function _validate_same_datos_aspirante_id()
    {
        if (!is_datos_aspirante_same($this->input->post('id'))) {
            $this->_response(['code' => 403, 'data' => ['mensaje' => 'No es el mismo registro Id.']]);
        }
    }
    private function _exists_datos_aspirante_id()
    {
        if (!is_datos_aspirante_id_in()) {
            return false;
        }
        return true;
    }

    private function _validate_session()
    {
        if (!is_registrado_in()) {
            $this->_response(['code' => 403, 'data' => ['mensaje' => 'No cuenta con una sesión activa.']]);
        }
    }
}
