<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Convocatorias extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		require_once APPPATH.'helpers/Sesiones_helper.php';
		$this->load->library('session');
		$this->load->model('convocatorias_model');
	}
	public function get_all_convocatorias()
	{
		$convocatororias =  $this->convocatorias_model->get_all_convocatorias();
		$this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode(['convocatorias' => $convocatororias], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
			->_display();
		exit;
	}
	public function get_convocatoria_by_id()
	{
		$convocatororia =  $this->convocatorias_model->get_convocatoria_by_id($this->session->convocatoria_id);
		if ($convocatororia === NULL) {
			$this->output
				->set_status_header(400)
				->set_content_type('application/json', 'utf-8')
				->set_output(json_encode([], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
				->_display();
			exit;
		} else {
			$this->output
				->set_status_header(200)
				->set_content_type('application/json', 'utf-8')
				->set_output(json_encode(['convocatoria' => $convocatororia], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
				->_display();
			exit;
		}
	}
}
