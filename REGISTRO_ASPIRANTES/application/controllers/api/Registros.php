<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Registros extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('America/Mexico_City');
		require_once APPPATH.'helpers/Sesiones_helper.php';
		$this->load->model('registros_model');
		$this->load->library('session');
		$_POST = json_decode(file_get_contents('php://input'), true);
	}

	public function start_regsitro()
	{
		$this->_validar_form_convocatoria();
		$this->session->convocatoria_id = $this->input->post("convocatoria_id");
		$session_data = array(
			'curso_id' => (int)$this->input->post("curso_id"),
			'pais_id' => (int)$this->input->post("pais_id"),
			'tipo_convocatoria_id' => (int)$this->input->post("tipo_convocatoria_id"),
			'logged_in' => TRUE
		);
		$this->session->set_userdata($session_data);
		$this->_response(['code' => 200, 'data' => []]);
	}
	public function registro_finish()
	{
		$this->_validate_session();
		$this->_validate_archvos_aspirante();
		if (!$this->registros_model->update($this->session->registro_id, ['status' => 2, 'updated_at' => date("Y-m-d H:i:s")])) {
			$this->_response(['code' => 500, 'data' => ['mensaje' => "No se finalizar el registro"]]);
		}
		$this->session->registro_end = true;
		$this->_response(['code' => 200, 'data' => []]);
	}

	private function _response($response)
	{
		$this->output
			->set_status_header($response['code'])
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response['data'], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
			->_display();
		exit;
	}


	private function _validar_form_convocatoria()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('', '');
		$this->form_validation->set_rules('convocatoria_id', 'Convocatoria', 'trim|required|is_natural_no_zero');
		$this->form_validation->set_rules('curso_id', 'Curso', 'trim|required|is_natural_no_zero');
		$this->form_validation->set_rules('pais_id', 'Pais', 'trim|required|is_natural_no_zero');
		$this->form_validation->set_rules('tipo_convocatoria_id', 'Tipo convocatoria', 'trim|required|is_natural_no_zero');

		if (!$this->form_validation->run()) {
			$this->_response(['code' => 400, 'data' => ['mensaje' => validation_errors()]]);
		}
	}

	private function _validate_archvos_aspirante()
	{
		$this->load->model('archivos_aspirantes_model');
		$array_archivos_usuario = $this->archivos_aspirantes_model->get_archivos_aspirante_by_id($this->session->archivos_aspirante_id);
		if (is_null($array_archivos_usuario)) {
			$this->_response(['code' => 500, 'data' => ['mensaje' => "No se finalizar el registro"]]);
		}
		unset(
			$array_archivos_usuario['id'],
			$array_archivos_usuario['status'],
			$array_archivos_usuario['created_at'],
			$array_archivos_usuario['updated_at'],
		);
		$array_archivos_convocatoria = $this->_get_array_archivos_by_tipo_convocatoria_and_pais_id();
		for ($i = 0; $i < count($array_archivos_convocatoria); $i++) {
			if (is_null($array_archivos_usuario[$array_archivos_convocatoria[$i]])) {
				$this->_response(['code' => 400, 'data' => ['mensaje' => '<p>Te faltan algunos archivos para concluir tú registro.</p>']]);
			}
		}
	}

	public function _get_array_archivos_by_tipo_convocatoria_and_pais_id()
	{
		$this->load->model('convocatorias_model');
		$convocatoria =  $this->convocatorias_model->get_convocatoria_by_id($this->session->convocatoria_id);
		switch ($convocatoria['tipo_convocatoria_id']) {
			case 1:
				if ((int)$this->session->pais_id == 1) {
					//directa nacional
					$arreglo =  array(
						'file_constancia_seleccionado_nacional',
						'file_solicitud_ingreso',
						'file_acta_nacimiento',
						'file_curp',
						'file_rfc',
						'file_identificacion_oficial',
						//'file_cartilla_militar',
						'file_comprobante_domicilio',
						'file_historial_academico_licenciatura_medicina',
						'file_terminacion_internado',
						'file_liberacion_servicio_social',
						'file_titulo_profecional',
						'file_cedula_profesional',
						'file_carta_motivos',
						'file_cartas_recomendacion',
						'file_certificado_medico',
						'file_cartilla_vacunacion',
						'file_cv',
						'file_recibo_pago_examen_psicometrico',
						'file_curso_especialidad',
						'file_carta_aceptacion_lineamientos',
						'file_ficha_salud',
						'file_fotografia'
					);
				} else {
					//directa extranjera
					$arreglo =  array(
						'file_constancia_seleccionado_nacional',
						'file_solicitud_ingreso',
						'file_acta_nacimiento',
						'file_pasaporte',
						'file_comprobante_domicilio',
						'file_historial_academico_licenciatura_medicina',
						'file_terminacion_internado',
						'file_liberacion_servicio_social',
						'file_titulo_profecional',
						'file_cedula_profesional',
						'file_carta_motivos',
						'file_cartas_recomendacion',
						'file_certificado_medico',
						'file_cartilla_vacunacion',
						'file_cv',
						'file_recibo_pago_examen_psicometrico',
						'file_curso_especialidad',
						'file_carta_aceptacion_lineamientos',
						'file_ficha_salud',
						'file_fotografia'
					);
				}
				break;
			case 2:
				if ((int)$this->session->pais_id == 1) {
					//indirecta nacional
					$arreglo =  array(
						'file_constancia_seleccionado_nacional',
						'file_solicitud_ingreso',
						'file_acta_nacimiento',
						'file_curp',
						'file_rfc',
						'file_identificacion_oficial',
						//'file_cartilla_militar',
						'file_comprobante_domicilio',
						'file_historial_academico_licenciatura_medicina',
						'file_terminacion_internado',
						'file_liberacion_servicio_social',
						'file_titulo_profecional',
						'file_cedula_profesional',
						'file_aval_academico_previos_posgrado',
						'file_carta_motivos',
						'file_cartas_recomendacion',
						'file_certificado_medico',
						'file_cartilla_vacunacion',
						'file_cv',
						'file_recibo_pago_examen_psicometrico',
						'file_curso_especialidad',
						'file_carta_buena_conducta',
						'file_carta_aceptacion_lineamientos',
						'file_ficha_salud',
						'file_fotografia'
					);
				} else {
					//indirecta extranjera
					$arreglo =  array(
						'file_constancia_seleccionado_nacional',
						'file_solicitud_ingreso',
						'file_acta_nacimiento',
						'file_pasaporte',
						'file_comprobante_domicilio',
						'file_historial_academico_licenciatura_medicina',
						'file_terminacion_internado',
						'file_liberacion_servicio_social',
						'file_titulo_profecional',
						'file_cedula_profesional',
						'file_aval_academico_previos_posgrado',
						'file_carta_motivos',
						'file_cartas_recomendacion',
						'file_certificado_medico',
						'file_cartilla_vacunacion',
						'file_cv',
						'file_recibo_pago_examen_psicometrico',
						'file_curso_especialidad',
						'file_carta_buena_conducta',
						'file_carta_aceptacion_lineamientos',
						'file_ficha_salud',
						'file_fotografia'
					);
				}
				break;
			case 3:
				if ((int)$this->session->pais_id == 1) {
					//alta nacional
					$arreglo =  array(
						'file_constancia_seleccionado_nacional',
						'file_solicitud_ingreso',
						'file_acta_nacimiento',
						'file_curp',
						'file_rfc',
						'file_identificacion_oficial',
						//'file_cartilla_militar',
						'file_comprobante_domicilio',
						'file_historial_academico_licenciatura_medicina',
						'file_terminacion_internado',
						'file_liberacion_servicio_social',
						'file_titulo_profecional',
						'file_cedula_profesional',
						'file_titulo_especialista',
						'file_carta_motivos',
						'file_cartas_recomendacion',
						'file_certificado_medico',
						'file_cartilla_vacunacion',
						'file_cv',
						'file_recibo_pago_examen_psicometrico',
						'file_curso_especialidad',
						'file_carta_aceptacion_lineamientos',
						'file_ficha_salud',
						'file_fotografia'
					);
				} else {
					$arreglo =  array(
						'file_constancia_seleccionado_nacional',
						'file_solicitud_ingreso',
						'file_acta_nacimiento',
						'file_pasaporte',
						'file_comprobante_domicilio',
						'file_historial_academico_licenciatura_medicina',
						'file_terminacion_internado',
						'file_liberacion_servicio_social',
						'file_titulo_profecional',
						'file_cedula_profesional',
						'file_titulo_especialista',
						'file_aval_academico_previos_posgrado',
						'file_carta_motivos',
						'file_cartas_recomendacion',
						'file_certificado_medico',
						'file_cartilla_vacunacion',
						'file_cv',
						'file_recibo_pago_examen_psicometrico',
						'file_curso_especialidad',
						'file_carta_buena_conducta',
						'file_carta_aceptacion_lineamientos',
						'file_ficha_salud',
						'file_fotografia',
						'file_pago_inscripcion'
					);
				}
				break;

				// default:
				// $arreglo =  array(
				// 	'file_constancia_seleccionado_nacional',
				// 	'file_solicitud_ingreso',
				// 	'file_acta_nacimiento',
				// 	'file_curp',
				// 	'file_rfc',
				// 	'file_pasaporte',
				// 	'file_identificacion_oficial',
				// 	'file_cartilla_militar',
				// 	'file_comprobante_domicilio',
				// 	'file_historial_academico_licenciatura_medicina',
				// 	'file_terminacion_internado',
				// 	'file_liberacion_servicio_social',
				// 	'file_titulo_profecional',
				// 	'file_cedula_profesional',
				// 	'file_titulo_especialista',
				// 	'file_cedula_especialista',
				// 	'file_aval_academico_previos_posgrado',
				// 	'file_carta_motivos',
				// 	'file_cartas_recomendacion',
				// 	'file_certificado_medico',
				// 	'file_cartilla_vacunacion',
				// 	'file_cv',
				// 	'file_constancia_solvencia_economica',
				// 	'file_recibo_pago_examen_psicometrico',
				// 	'file_curso_especialidad',
				// 	'file_carta_buena_conducta',
				// 	'file_carta_aceptacion_lineamientos',
				// 	'file_eval_psicometrica_psicologica',
				// 	'file_ficha_salud',
				// 	'file_fotografia',
				// 	'file_pago_inscripcion'
				// );
				// 	break;
		}
		return $arreglo;
	}

	private function _validate_session()
	{
		if (!is_logged_in()) {
			$this->_response(['code' => 403, 'data' => ['mensaje' => 'No cuenta con una sesión activa.']]);
		}
	}
}
