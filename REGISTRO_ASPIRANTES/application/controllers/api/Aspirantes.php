<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Aspirantes extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('America/Mexico_City');
		require_once APPPATH.'helpers/Sesiones_helper.php';
		$this->load->library('session');
		$this->load->model('aspirantes_model');
		$_POST = json_decode(file_get_contents('php://input'), true);
	}

	public function get_aspirante_by_curp()
	{
		$this->_validate_session();
		$this->_validate_curp();
		$aspirante = $this->aspirantes_model->get_aspirante_by_curp($this->input->post('curp'));
		if ($aspirante === null) {
			$this->_response(['code' => 404, 'data' => []]);
		}
		$this->session->aspirante_id = $aspirante['id'];
		$this->session->pais_id = $aspirante['pais_id'];
		$this->_response(['code' => 200, 'data' => ['aspirante' => $aspirante]]);
	}

	public function get_aspirante_by_pasaporte()
	{
		$this->_validate_session();
		$this->_validate_pasaporte();
		$aspirante = $this->aspirantes_model->get_aspirante_by_pasaporte($this->input->post('pasaporte'));
		if ($aspirante === null) {
			$this->_response(['code' => 404, 'data' => []]);
		}
		$this->session->aspirante_id = $aspirante['id'];
		$this->session->pais_id = $aspirante['pais_id'];
		$this->_response(['code' => 200, 'data' => ['aspirante' => $aspirante]]);
	}

	//------------------------------- crear aspirante nacional 
	public function create_nacional_post()
	{
		$this->_validate_session();
		$this->_form_validation_nacional_create();
		$this->db->trans_start();
		$id = $this->aspirantes_model->insert($this->_fill_data_nacional_create());
		if (!$id) {
			$this->_response(['code' => 500, 'data' => ['mensaje' => "No se logro insertar los datos."]]);
		}
		$this->session->aspirante_id = $id;
		if (!$this->_exist_registro()) {
			if (!$this->_insert_registro()) {
				$this->_response(['code' => 500, 'data' => ['mensaje' => "No se logro insertar los datos."]]);
			}
		}
		$this->db->trans_complete();
		$this->_response(['code' => 200, 'data' => ['mensaje' => 'Se guardaron los datos.']]);
	}
	//------------------------------- crear aspirante extranjero 
	public function create_extranjero_post()
	{
		$this->_validate_session();
		$this->_form_validation_extranjero_create();
		$this->db->trans_start();
		$id = $this->aspirantes_model->insert($this->_fill_data_extranjero_create());
		if (!$id) {
			$this->_response(['code' => 500, 'data' => ['mensaje' => "No se logro insertar los datos."]]);
		}
		$this->session->aspirante_id = $id;
		if (!$this->_exist_registro()) {
			if (!$this->_insert_registro()) {
				$this->_response(['code' => 500, 'data' => ['mensaje' => "No se logro insertar los datos."]]);
			}
		}
		$this->db->trans_complete();
		$this->_response(['code' => 200, 'data' => ['mensaje' => 'Se guardaron los datos.']]);
	}
	//------------------------------- actualizar aspirante nacional
	public function update_nacional_post()
	{
		$this->_validate_session();
		$this->_aspirante_id_validation();
		$this->_validate_apirante_id_same();
		$this->_form_validation_nacional_update();
		$this->db->trans_start();
		$id = $this->aspirantes_model->update($this->input->get('id'), $this->_fill_data_nacional_update());
		if (!$id) {
			$this->_response(['code' => 500, 'data' => ['mensaje' => "No se logro actualizar los datos."]]);
		}
		$this->session->aspirante_id = $id;
		if (!$this->_exist_registro()) {
			if (!$this->_insert_registro()) {
				$this->_response(['code' => 500, 'data' => ['mensaje' => "No se logro actualizar los datos."]]);
			}
		}
		$this->db->trans_complete();
		$this->_response(['code' => 200, 'data' => ['mensaje' => 'Se actualizaron los datos.']]);
	}
	//------------------------------- actualizar aspirante extranjero
	public function update_extranjero_post()
	{
		$this->_validate_session();
		$this->_aspirante_id_validation();
		$this->_validate_apirante_id_same();
		$this->_form_validation_extrajero_update();
		$this->db->trans_start();
		$id = $this->aspirantes_model->update($this->input->get('id'), $this->_fill_data_extranjero_update());
		if (!$id) {
			$this->_response(['code' => 500, 'data' => ['mensaje' => "No se logro actualizar los datos."]]);
		}
		$this->session->aspirante_id = $id;
		if (!$this->_exist_registro()) {
			if (!$this->_insert_registro()) {
				$this->_response(['code' => 500, 'data' => ['mensaje' => "No se logro actualizar los datos."]]);
			}
		}
		$this->db->trans_complete();
		$this->_response(['code' => 200, 'data' => ['mensaje' => 'Se actualizaron los datos.']]);
	}

	public function contiene_digitos($cadena)
	{
		for ($index = 0; $index < strlen($cadena) - 1; $index++) {
			if (is_numeric($cadena[$index])) {
				$this->form_validation->set_message('contiene_digitos', 'El  campo {field} no debe contiener digitos');
				return FALSE;
			}
		}
		return true;
	}

	private function _insert_registro()
	{
		$this->load->model('registros_model');
		$registro_id = $this->registros_model->insert($this->_fill_data_registro());
		if ($registro_id !== NULL) {
			$this->session->registro_id = $registro_id;
			return true;
		} else {
			return false;
		}
	}

	private function _exist_registro()
	{
		$this->load->model('registros_model');
		$registro = $this->registros_model->get_registro_id_by_aspirante_convocatoria_curso_ids($this->session->aspirante_id, $this->session->convocatoria_id, $this->session->curso_id);
		if ($registro !== NULL) {
			$this->session->registro_id = $registro['id'];
			if ((int)$registro['status'] !== 1) {
				$this->session->registro_end = true;
			} else {
				$this->session->registro_end = false;
			}
			return true;
		} else {
			return false;
		}
	}

	private function _response($response)
	{
		$this->output
			->set_status_header($response['code'])
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response['data'], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
			->_display();
		exit;
	}


	private function _fill_data_registro()
	{
		$data_insert['convocatoria_id'] = $this->session->convocatoria_id;
		$data_insert['aspirante_id'] = 	$this->session->aspirante_id;
		$data_insert['curso_id'] = 	$this->session->curso_id;
		$data_insert['status'] = 1;
		$data_insert['created_at'] = date("Y-m-d H:i:s");
		return $data_insert;
	}

	//-------------------------Llenado del array para aspirantes nacionales, created y updated
	private function _fill_data_nacional_create()
	{
		$data_insert['pais_id'] = $this->session->pais_id;
		$data_insert['nacimiento_entidad_id'] =  $_POST["nacimiento_entidad_id"];
		$data_insert['nombre'] = strtoupper($this->input->post('nombre'));
		$data_insert['apellido_1'] = strtoupper($this->input->post('apellido_1'));
		$data_insert['apellido_2'] = (isset($_POST["apellido_2"]) && $_POST["apellido_2"] !== '') ? strtoupper($_POST["apellido_2"]) : null;
		$data_insert['genero'] = $this->input->post('genero');
		$data_insert['fecha_nacimiento'] = $this->input->post('fecha_nacimiento');
		$data_insert['curp'] = strtoupper($this->input->post('curp'));
		$data_insert['rfc'] = (isset($_POST["rfc"]) && !empty($_POST["rfc"])) ? strtoupper($_POST["rfc"]) : null;
		//$data_insert['status'] = $this->input->post('status');
		$data_insert['status'] = 1;
		$data_insert['created_at'] = date("Y-m-d H:i:s");
		return $data_insert;
	}
	private function _fill_data_nacional_update()
	{
		// $data_insert['pais_id'] = $this->session->pais_id;
		// $data_insert['nacimiento_entidad_id'] =  $_POST["nacimiento_entidad_id"];
		// $data_insert['nombre'] = strtoupper($this->input->post('nombre'));
		// $data_insert['apellido_1'] = strtoupper($this->input->post('apellido_1'));
		$data_insert['apellido_2'] = (isset($_POST["apellido_2"]) && $_POST["apellido_2"] !== '') ? strtoupper($_POST["apellido_2"]) : null;
		// $data_insert['genero'] = $this->input->post('genero');
		// $data_insert['fecha_nacimiento'] = $this->input->post('fecha_nacimiento');
		// $data_insert['curp'] = strtoupper($this->input->post('curp'));
		$data_insert['rfc'] = (isset($_POST["rfc"]) && !empty($_POST["rfc"])) ? strtoupper($_POST["rfc"]) : null;
		//$data_insert['status'] = $this->input->post('status');
		$data_insert['updated_at'] = date("Y-m-d H:i:s");
		return $data_insert;
	}
	//-------------------------Llenado del array para aspirantes extranjeros, created y updated
	private function _fill_data_extranjero_create()
	{
		$data_insert['pais_id'] = $this->session->pais_id;
		$data_insert['nombre'] = strtoupper($this->input->post('nombre'));
		$data_insert['apellido_1'] = strtoupper($this->input->post('apellido_1'));
		$data_insert['apellido_2'] = (isset($_POST["apellido_2"]) && $_POST["apellido_2"] !== '') ? strtoupper($_POST["apellido_2"]) : null;
		$data_insert['genero'] = $this->input->post('genero');
		$data_insert['fecha_nacimiento'] = $this->input->post('fecha_nacimiento');
		$data_insert['pasaporte'] = strtoupper($this->input->post('pasaporte'));
		//$data_insert['status'] = $this->input->post('status');
		$data_insert['status'] = 1;
		$data_insert['created_at'] = date("Y-m-d H:i:s");
		return $data_insert;
	}

	private function _fill_data_extranjero_update()
	{
		//$data_insert['pais_id'] = $this->session->pais_id;
		//$data_insert['nombre'] = strtoupper($this->input->post('nombre'));
		//$data_insert['apellido_1'] = strtoupper($this->input->post('apellido_1'));
		$data_insert['apellido_2'] = (isset($_POST["apellido_2"]) && $_POST["apellido_2"] !== '') ? strtoupper($_POST["apellido_2"]) : null;
		//$data_insert['genero'] = $this->input->post('genero');
		//$data_insert['fecha_nacimiento'] = $this->input->post('fecha_nacimiento');
		//$data_insert['pasaporte'] = strtoupper($this->input->post('pasaporte'));
		//$data_insert['status'] = $this->input->post('status');
		$data_insert['updated_at'] = date("Y-m-d H:i:s");
		return $data_insert;
	}

	//-------------------------Validacion del formulario para aspirantes nacionales, created y updated
	private function _form_validation_nacional_create()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('', '');
		$this->form_validation->set_rules('nacimiento_entidad_id', 'Entidad Federativa', 'trim|required|is_natural_no_zero');
		$this->form_validation->set_rules('nombre', 'Nombres(s)', 'trim|required|max_length[100]|callback_contiene_digitos');
		$this->form_validation->set_rules('apellido_1', 'Apellido Paterno', 'trim|required|max_length[100]|callback_contiene_digitos');
		$this->form_validation->set_rules('apellido_2', 'Apellido Materno', 'trim|max_length[100]|callback_contiene_digitos');
		$this->form_validation->set_rules('genero', 'Genero', 'trim|required|is_natural_no_zero|in_list[1,2]');
		$this->form_validation->set_rules('fecha_nacimiento', 'Fecha de Nacimiento', 'trim|required|Date');
		$this->form_validation->set_rules('curp', 'CURP', 'trim|required|alpha_numeric|exact_length[18]|is_unique[aspirantes.curp]');
		$this->form_validation->set_rules('rfc', 'RFC', 'trim|alpha_numeric|min_length[12]|max_length[13]|is_unique[aspirantes.rfc]');
		if (!$this->form_validation->run()) {
			$this->_response(['code' => 400, 'data' => ['mensaje' => validation_errors()]]);
		}
	}

	private function _form_validation_nacional_update()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('', '');
		$this->form_validation->set_rules('nacimiento_entidad_id', 'Entidad Federativa', 'trim|required|is_natural_no_zero');
		$this->form_validation->set_rules('nombre', 'Nombres(s)', 'trim|required|max_length[100]|callback_contiene_digitos');
		$this->form_validation->set_rules('apellido_1', 'Apellido Paterno', 'trim|required|max_length[100]|callback_contiene_digitos');
		$this->form_validation->set_rules('apellido_2', 'Apellido Materno', 'trim|max_length[100]|callback_contiene_digitos');
		$this->form_validation->set_rules('genero', 'Genero', 'trim|required|is_natural_no_zero|in_list[1,2]');
		$this->form_validation->set_rules('fecha_nacimiento', 'Fecha de Nacimiento', 'trim|required|Date');
		$this->form_validation->set_rules('curp', 'CURP', 'trim|required|alpha_numeric|exact_length[18]');
		$this->form_validation->set_rules('rfc', 'RFC', 'trim|alpha_numeric|min_length[12]|max_length[13]');
		if (!$this->form_validation->run()) {
			$this->_response(['code' => 400, 'data' => ['mensaje' => validation_errors()]]);
		}
	}



	//------------------------Validacion del formulario para aspirantes extranjeros, created y updated
	private function _form_validation_extranjero_create()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('', '');
		$this->form_validation->set_rules('nombre', 'Nombres(s)', 'trim|required|max_length[100]|callback_contiene_digitos');
		$this->form_validation->set_rules('apellido_1', 'Apellido Paterno', 'trim|required|max_length[100]|callback_contiene_digitos');
		$this->form_validation->set_rules('apellido_2', 'Apellido Materno', 'trim|max_length[100]|callback_contiene_digitos');
		$this->form_validation->set_rules('genero', 'Genero', 'trim|required|is_natural_no_zero|in_list[1,2]');
		$this->form_validation->set_rules('fecha_nacimiento', 'Fecha de Nacimiento', 'trim|required|Date');
		$this->form_validation->set_rules('pasaporte', 'Pasaporte', 'trim|required|alpha_numeric|min_length[9]|max_length[15]|is_unique[aspirantes.pasaporte]');
		if (!$this->form_validation->run()) {
			$this->_response(['code' => 400, 'data' => ['mensaje' => validation_errors()]]);
		}
	}
	private function _form_validation_extrajero_update()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('', '');
		$this->form_validation->set_rules('nombre', 'Nombres(s)', 'trim|required|max_length[100]|callback_contiene_digitos');
		$this->form_validation->set_rules('apellido_1', 'Apellido Paterno', 'trim|required|alpha|max_length[100]|callback_contiene_digitos');
		$this->form_validation->set_rules('apellido_2', 'Apellido Materno', 'trim|alpha|max_length[100]|callback_contiene_digitos');
		$this->form_validation->set_rules('genero', 'Genero', 'trim|required|is_natural_no_zero|in_list[1,2]');
		$this->form_validation->set_rules('fecha_nacimiento', 'Fecha de Nacimiento', 'trim|required|Date');
		$this->form_validation->set_rules('pasaporte', 'Pasaporte', 'trim|required|alpha_numeric|min_length[9]|max_length[15]');
		if (!$this->form_validation->run()) {
			$this->_response(['code' => 400, 'data' => ['mensaje' => validation_errors()]]);
		}
	}

	private function _aspirante_id_validation()
	{
		if (isset($_GET["id"])) {
			if (!is_null($_GET["id"]) || $_GET["id"] !== '') {
				if (is_numeric($_GET["id"])) {
					return true;
				}
				$this->_response(['code' => 400, 'data' => ['mensaje' => 'No es un numero la variable']]);
			}
			$this->_response(['code' => 400, 'data' => ['mensaje' => 'Esta vacia la variable']]);
		}
		$this->_response(['code' => 400, 'data' => ['mensaje' => 'No se ha iniciado la viarible.']]);
	}
	private function _validate_curp()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('curp', 'CURP', 'trim|required|alpha_numeric|exact_length[18]');
		if (!$this->form_validation->run()) {
			$this->_response(['code' => 400, 'data' => ['mensaje' => validation_errors()]]);
		}
	}
	private function _validate_pasaporte()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('pasaporte', 'Pasaporte', 'trim|required|alpha_numeric|min_length[9]|max_length[15]');
		if (!$this->form_validation->run()) {
			$this->_response(['code' => 400, 'data' => ['mensaje' => validation_errors()]]);
		}
	}
	private function _validate_apirante_id_same()
	{
		if (!is_aspirante_same($this->input->get('id'))) {
			$this->_response(['code' => 403, 'data' => ['mensaje' => 'El aspirante no cuerda con el registro.']]);
		}
	}

	private function _validate_session()
	{
		if (!is_logged_in()) {
			$this->_response(['code' => 403, 'data' => ['mensaje' => 'Tú registro se vío interrumpido.']]);
		}
	}
}
