<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Archivos_aspirantes extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
       require_once APPPATH.'helpers/Sesiones_helper.php';
        $this->load->helper('url');
        $this->load->model('archivos_aspirantes_model');
        $this->load->library('session');
        //$_POST = json_decode(file_get_contents('php://input'), true);
    }

    public function get_file()
    {
        $this->_validate_session();
        $this->_validate_select();
        $carpeta = $this->_get_carpeta();
        $archivo = $this->_get_name_archivo();
        $ruta =  '../../documentos/' . $carpeta . '/' . $archivo;

        if (!file_exists($ruta)) {
            $this->_response(['code' => 400, 'data' => []]);
        }
        $this->_response(['code' => 200, 'data' => ['mensaje' => '../' . $ruta]]);
    }

    public function get_files_exists()
    {
        $this->_validate_session();
        $files = $this->archivos_aspirantes_model->get_archivos_aspirante_by_id($this->session->archivos_aspirante_id);
        unset(
            $files['id'],
            $files['status'],
            $files['created_at'],
            $files['updated_at']
        );
        $files_array = array();
        $indice = 1;
        foreach ($files as $file) {
            if (!empty($file)) {
                array_push($files_array, $indice);
            }
            $indice++;
        }
        $this->_response(['code' => 200, 'data' => ['array' => $files_array]]);
    }

    public function create_carpeta()
    {
        $this->_validate_session();
        $this->_get_archivo_aspirante_id();
        $carpeta = $this->_get_carpeta();
        $path = '../../documentos/' . $carpeta;
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }
        $this->_response(['code' => 200, 'data' => []]);
    }

    public function uplod_file()
    {
        $this->_validate_session();
        $this->_validate_select();
        $this->db->trans_start();
        $this->_update_archivo_aspirante();
        $this->_uplod_file();
        $this->db->trans_complete();
        $this->_response(['code' => 200, 'data' => ['mensaje' => 'Se subio él archivo.']]);
    }

    public function remove_file()
    {
        $this->_validate_session();
        $this->_validate_select();
        $carpeta = $this->_get_carpeta();
        $archivo = $this->_get_name_archivo();
        $ruta =  '../../documentos/' . $carpeta . '/' . $archivo;
        if (!file_exists($ruta)) {
            $this->_response(['code' => 404, 'data' => ['mensaje' => 'Este archivo no existe.']]);
        }
        $this->db->trans_start();
        $this->_remove_archivo_aspirante();
        if (!unlink($ruta)) {
            $this->_response(['code' => 500, 'data' => ['mensaje' => 'Ocurrio un error, no se logro remover el archvio.']]);
        }
        $this->db->trans_complete();
        $this->_response(['code' => 200, 'data' => ['mensaje' => 'Se removio él archivo.']]);
    }

    private function _response($response)
    {
        $this->output
            ->set_status_header($response['code'])
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response['data'], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
            ->_display();
        exit;
    }

    private function _get_archivo_aspirante_id()
    {
        $this->load->model('registros_model');
        $archivos_aspirante_id = $this->registros_model->get_archivos_aspirante_id_by_registro_id($this->session->registro_id);
        if (is_null($archivos_aspirante_id)) {
            $archivos_aspirante_id = $this->archivos_aspirantes_model->insert(['status' => 1, 'created_at' => date("Y-m-d H:i:s")]);
            if (!$archivos_aspirante_id) {
                $this->_response(['code' => 500, 'data' => ['mensaje' => 'Ocurrio un problema por favor intentalo más tarde.']]);
            }
            if (!$this->registros_model->update($this->session->registro_id, ['archivos_aspirante_id' => $archivos_aspirante_id])) {
                $this->_response(['code' => 500, 'data' => ['mensaje' => 'Ocurrio un problema por favor intentalo más tarde.']]);
            }
            $this->session->archivos_aspirante_id = $archivos_aspirante_id;
        } else {
            $this->session->archivos_aspirante_id = $archivos_aspirante_id;
        }
    }

    private function _update_archivo_aspirante()
    {
        $archivo_update = 'file_' .  $this->_select_archivo();
        $nombre_archivo = $this->_get_name_archivo();
        if (!$this->archivos_aspirantes_model->update($this->session->archivos_aspirante_id, [$archivo_update => $nombre_archivo, 'updated_at' => date("Y-m-d H:i:s")])) {
            $this->_response(['code' => 500, 'data' => ['mensaje' => 'Ocurrio un problema por favor intentalo más tarde. ']]);
        }
    }

    private function _remove_archivo_aspirante()
    {
        $archivo_update = 'file_' .  $this->_select_archivo();
        if (!$this->archivos_aspirantes_model->update($this->session->archivos_aspirante_id, [$archivo_update => NULL, 'updated_at' => date("Y-m-d H:i:s")])) {
            $this->_response(['code' => 500, 'data' => ['mensaje' => 'Ocurrio un problema por favor intentalo más tarde.']]);
        }
    }

    private function _uplod_file()
    {
        if (isset($_FILES['archivo'])) {
            $_FILES['archivo']['name'] = $this->_get_name_archivo();
            $ruta = $this->_get_carpeta();
            $config['upload_path'] = '../../DOCUMENTOS/' . $ruta;
            $config['allowed_types'] = 'pdf';
            $config['max_size'] = 10000;
            $config['max_width'] = 1024;
            $config['max_height'] = 768;
            $config['overwrite'] = true;
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('archivo')) {
                $this->_response(['code' => 400, 'data' => ['mensaje' => $this->upload->display_errors()]]);
            }
        }
    }

    private function _select_archivo()
    {
        $archivo = '';
        switch ($this->input->post('select')) {
            case 1:
                $archivo = 'constancia_seleccionado_nacional';
                break;
            case 2:
                $archivo = 'solicitud_ingreso';
                break;
            case 3:
                $archivo = 'acta_nacimiento';
                break;
            case 4:
                $archivo = 'curp';
                break;
            case 5:
                $archivo = 'rfc';
                break;
            case 6:
                $archivo = 'pasaporte';
                break;
            case 7:
                $archivo = 'identificacion_oficial';
                break;
            case 8:
                $archivo = 'cartilla_militar';
                break;
            case 9:
                $archivo = 'comprobante_domicilio';
                break;
            case 10:
                $archivo = 'historial_academico_licenciatura_medicina';
                break;
            case 11:
                $archivo = 'terminacion_internado';
                break;
            case 12:
                $archivo = 'liberacion_servicio_social';
                break;
            case 13:
                $archivo = 'titulo_profecional';
                break;
            case 14:
                $archivo = 'cedula_profesional';
                break;
            case 15:
                $archivo = 'titulo_especialista';
                break;
            case 16:
                $archivo = 'cedula_especialista';
                break;
            case 17:
                $archivo = 'aval_academico_previos_posgrado';
                break;
            case 18:
                $archivo = 'carta_motivos';
                break;
            case 19:
                $archivo = 'cartas_recomendacion';
                break;
            case 20:
                $archivo = 'certificado_medico';
                break;
            case 21:
                $archivo = 'cartilla_vacunacion';
                break;
            case 22:
                $archivo = 'cv';
                break;
            case 23:
                $archivo = 'constancia_solvencia_economica';
                break;
            case 24:
                $archivo = 'recibo_pago_examen_psicometrico';
                break;
            case 25:
                $archivo = 'curso_especialidad';
                break;
            case 26:
                $archivo = 'carta_buena_conducta';
                break;
            case 27:
                $archivo = 'carta_aceptacion_lineamientos';
                break;
            case 28:
                $archivo = 'eval_psicometrica_psicologica';
                break;
            case 29:
                $archivo = 'ficha_salud';
                break;
            case 29:
                $archivo = 'ficha_salud';
                break;
            case 30:
                $archivo = 'fotografia';
                break;
            case 31:
                $archivo = 'pago_inscripcion';
                break;


                // default:
                //     $archivo = 'INE';
                //     break;
        }
        return $archivo;
    }

    private function _get_name_archivo()
    {
        $this->load->model('aspirantes_model');
        $archivo = $this->_select_archivo();
        if ($this->session->pais_id == 1) {
            $aspirante =  $this->aspirantes_model->get_aspirante_curp_by_id($this->session->aspirante_id);
            $file = $aspirante['curp'] . '_' . $archivo . '.pdf';
        } else {
            $aspirante =  $this->aspirantes_model->get_aspirante_pasaporte_by_id($this->session->aspirante_id);
            $file = $aspirante['pasaporte'] . '_' . $archivo . '.pdf';
        }
        return str_replace(' ', '_', $file);
    }

    private function _get_carpeta()
    {
        $this->load->model('convocatorias_model');
        $convocatororia =  $this->convocatorias_model->get_convocatoria_by_id($this->session->convocatoria_id);
        $this->load->model('cursos_model');
        $curso =  $this->cursos_model->get_curso_by_id($this->session->curso_id);
        $this->load->model('aspirantes_model');
        if ($this->session->pais_id == 1) {
            $aspirante =  $this->aspirantes_model->get_aspirante_curp_by_id($this->session->aspirante_id);
            $carpeta = $convocatororia['nombre_convocatoria'] . '/' . $curso['curso'] . '/' . $aspirante['curp'];
        } else {
            $aspirante =  $this->aspirantes_model->get_aspirante_pasaporte_by_id($this->session->aspirante_id);
            $carpeta = $convocatororia['nombre_convocatoria'] . '/' . $curso['curso'] . '/' . $aspirante['pasaporte'];
        }
        return str_replace(' ', '', $carpeta);
    }

    private function _validate_select()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('', '');
        $this->form_validation->set_rules('select', 'Select', 'trim|required|numeric');
        if (!$this->form_validation->run()) {
            $this->_response(['code' => 400, 'data' => ['mensaje' => validation_errors()]]);
        }
    }

    private function _validate_session()
    {
        if (!is_logged_in()) {
            $this->_response(['code' => 403, 'data' => ['mensaje' => 'No cuenta con una sesión activa.']]);
        }
    }
}
