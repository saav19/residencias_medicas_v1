<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Codigos_postales extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('America/Mexico_City');
		$this->load->model('codigos_postales_model');
		$_POST = json_decode(file_get_contents('php://input'), true);
	}
	public function get_direccion_by_codigo_postal()
	{
		$direccion = $this->codigos_postales_model->get_direccion_by_codigo_postal($this->input->post('codigo_postal'));
		if ($direccion !== []) {
			$this->_response(['code' => 200, 'data' => ['direccion' => $direccion]]);
		}
	}
	public function get_codigo_postal_by_codigo_postal_id()
	{
		$codigo_postal = $this->codigos_postales_model->get_codigo_postal_by_codigo_postal_id($this->input->post('codigo_postal_id'));
		if ($codigo_postal === NULL) {
			$this->_response(['code' => 404, 'data' => []]);
		}
		$this->_response(['code' => 200, 'data' => ['codigo_postal' => $codigo_postal]]);
	}

	private function _response($response)
	{
		$this->output
			->set_status_header($response['code'])
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response['data'], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
			->_display();
		exit;
	}
}
