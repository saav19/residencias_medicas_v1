<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Universidades extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('universidades_model');
    }
    
    public function get_all_universidades()
    {
        $universidades = $this->universidades_model->get_all_universidades();
        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode(['universidades' => $universidades], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
            ->_display();
        exit;
    }
}
