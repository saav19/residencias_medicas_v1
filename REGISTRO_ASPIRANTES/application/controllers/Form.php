<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Form extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		require_once APPPATH.'helpers/Sesiones_helper.php';
		$this->load->helper('url');
		$this->load->library('session');
	}

	public function form_general()
	{
		if (is_logged_in()) {
			if ((int)$this->session->pais_id === 1) {
				$data = ['template' => 2, 'document_title' => 'FORM::DATOS-GENERALES'];
				$this->load->view('Main_view.php', $data);
			} else {
				$data = ['template' => 3, 'document_title' => 'FORM::DATOS-GENERALES'];
				$this->load->view('Main_view.php', $data);
			}
		} else {
			$this->close();
		}
	}

	public function form_particular()
	{
		if (is_registrado_in()) {
			if (!is_registro_end()) {
				switch ((int)$this->session->tipo_convocatoria_id) {
					case 1:
						if ((int)$this->session->pais_id === 1) {
							$data = ['template' => 4, 'document_title' => 'FORM::DATOS-PARTICULARES'];
							$this->load->view('Main_view.php', $data);
						} else {
							$data = ['template' => 5, 'document_title' => 'FORM::DATOS-PARTICULARES'];
							$this->load->view('Main_view.php', $data);
						}
						break;
					case 2:
						if ((int)$this->session->pais_id === 1) {
							$data = ['template' => 6, 'document_title' => 'FORM::DATOS-PARTICULARES'];
							$this->load->view('Main_view.php', $data);
						} else {
							$data = ['template' => 7, 'document_title' => 'FORM::DATOS-PARTICULARES'];
							$this->load->view('Main_view.php', $data);
						}
						break;
					case 3:
						if ((int)$this->session->pais_id === 1) {
							$data = ['template' => 8, 'document_title' => 'FORM::DATOS-PARTICULARES'];
							$this->load->view('Main_view.php', $data);
						} else {
							$data = ['template' => 9, 'document_title' => 'FORM::DATOS-PARTICULARES'];
							$this->load->view('Main_view.php', $data);
						}
						break;
					default:
						# code...
						break;
				}
			} else {
				$data = ['template' => 16, 'document_title' => 'FORM::REGISTRO-END'];
				$this->load->view('Main_view.php', $data);
			}
		} else {
			$this->close();
		}
	}

	public function form_archivos()
	{
		if (is_registrado_in()) {
			if (!is_registro_end()) {
				switch ((int)$this->session->tipo_convocatoria_id) {
					case 1:
						if ((int)$this->session->pais_id === 1) {
							$data = ['template' => 10, 'document_title' => 'FORM::ARCHIVOS'];
							$this->load->view('Main_view.php', $data);
						} else {
							$data = ['template' => 11, 'document_title' => 'FORM::ARCHIVOS'];
							$this->load->view('Main_view.php', $data);
						}
						break;
					case 2:
						if ((int)$this->session->pais_id === 1) {
							$data = ['template' => 12, 'document_title' => 'FORM::ARCHIVOS'];
							$this->load->view('Main_view.php', $data);
						} else {
							$data = ['template' => 13, 'document_title' => 'FORM::ARCHIVOS'];
							$this->load->view('Main_view.php', $data);
						}
						break;
					case 3:
						if ((int)$this->session->pais_id === 1) {
							$data = ['template' => 14, 'document_title' => 'FORM::ARCHIVOS'];
							$this->load->view('Main_view.php', $data);
						} else {
							$data = ['template' => 15, 'document_title' => 'FORM::ARCHIVOS'];
							$this->load->view('Main_view.php', $data);
						}
						break;
					default:
						# code...
						break;
				}
			} else {
				$data = ['template' => 16, 'document_title' => 'FORM::REGISTRO-END'];
				$this->load->view('Main_view.php', $data);
			}
		} else {
			$this->close();
		}
	}
	public function form_end()
	{
		if (is_registro_end()) {
			$data = ['template' => 16, 'document_title' => 'FORM::REGISTRO-END'];
			$this->load->view('Main_view.php', $data);
		} else {
			$this->close();
		}
	}

	public function close()
	{
		header("Location:" . base_url());
		$this->session->unset_userdata("session_data");
		$this->session->sess_destroy();
		die();
	}
}
