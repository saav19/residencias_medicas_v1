<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Main extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
	}
	public function index()
	{
		$this-> _destroy_session_previa();
		$data = ['template' => 1, 'document_title' => 'REGISTRO-ASPIRANTES'];
		$this->load->view('Main_view.php', $data);
	}

	private function _destroy_session_previa()
	{
		if (session_status() === PHP_SESSION_NONE) {
			$this->load->library('session');
			$this->session->unset_userdata("session_data");
			$this->session->sess_destroy();
		}
	}
}
