function notify(mensaje, type) {
    new Noty({
        type: type,
        layout: 'topRight',
        theme: 'metroui',
        text: '<div><h5><strong>' + mensaje + '</strong></h5></div>',
    }).show().setTimeout(2000);
}
