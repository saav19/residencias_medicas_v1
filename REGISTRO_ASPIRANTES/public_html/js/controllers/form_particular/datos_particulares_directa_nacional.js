
app.controller('DatosParticularesCtrl',
    function ($scope, $http, $window) {
        $scope.form = {
            id: 0,
            codigo_postal_id: '',
            domicilio_calle: '',
            domicilio_numero_exterior: '',
            domicilio_numero_interior: '',
            domicilio_entre_calle_1: '',
            domicilio_entre_calle_2: '',
            correo_electronico: '',
            comfirm_correo_electronico: '',
            telefono_fijo: '',
            telefono_movil: '',
            estado_civil: '',
            grupo_sanguineo: '',
            factor_rh: '',
            talla_bata: '',
            talla_uniforme_quirurgico: '',
            talla_pantalon: '',
            talla_zapatos: '',
            folio_enarm: '',
            cedula_profesional: '',
            ano_egreso_licenciatura: '',
            promedio_licenciatura: '',
            status: 1
        };
        $scope.entidades = [];
        $scope.colonias = [];
        $scope.disabled_colonia = true;
        select_municipio = '';
        select_estado = '';
        $scope.find_codigo_postal = '';
        $scope.nombre_convocatoria = '';
        $scope.curso = [];
        $scope.button_disabled = false;
        get_datos_aspirante();

        function get_datos_aspirante() {
            $http({
                method: "GET",
                url: api_url + "datos_aspirantes/get_datos_aspirante_by_id"
            }).then(function (response) {
                $scope.form = response.data.datos_aspirante;
                $scope.form.comfirm_correo_electronico = response.data.datos_aspirante.correo_electronico;
                $http({
                    method: "POST",
                    url: api_url + "codigos_postales/get_codigo_postal_by_codigo_postal_id",
                    data: { codigo_postal_id: $scope.form.codigo_postal_id }
                }).then(function (response) {
                    $scope.find_codigo_postal = response.data.codigo_postal.codigo_postal;
                    $scope.get_domicilio_by_codigo_postal();
                });
            });
        }

        $scope.submit = function () {
            $scope.button_disabled = true;
            var data_send = angular.copy($scope.form);
            $http({
                method: "POST",
                url: api_url + 'datos_aspirantes/directa_nacional_post',
                data: data_send
            }).then(function (response) {
                notify(response.data.mensaje, 'success');
                setTimeout(function () {
                    $window.location = base_url + 'form/form_archivos';
                }, 1000);
            }).catch(function (err) {
                if (err.status == 500) {
                    $scope.button_disabled = false;
                    notify(err.data.mensaje, 'error');
                } else if (err.status == 400) {
                    $scope.button_disabled = false;
                    notify(err.data.mensaje, 'error');
                } else if (err.status == 404) {
                    $scope.button_disabled = false;
                    notify(err.data.mensaje, 'error');
                }
            });
        }

        $scope.get_domicilio_by_codigo_postal = function () {
            var codigo_postal = angular.copy($scope.find_codigo_postal);
            if (codigo_postal != null) {
                if (codigo_postal.length == 5) {
                    $http({
                        method: "POST",
                        url: api_url + "codigos_postales/get_direccion_by_codigo_postal",
                        data: { codigo_postal: codigo_postal }
                    }).then(function (response) {
                        $scope.colonias = response.data.direccion;
                        if (response.data.direccion.length == 1) {
                            $scope.form.codigo_postal_id = response.data.direccion[0]['id'];
                        } else {
                            $scope.disabled_colonia = false;
                        }
                        $scope.select_municipio = response.data.direccion[0]['municipio'];
                        $scope.select_estado = response.data.direccion[0]['estado'];
                    }).catch(function (err) {

                    });
                } else if (codigo_postal.length < 5) {
                    $scope.colonias = [];
                    $scope.disabled_colonia = true;
                    $scope.select_municipio = '';
                    $scope.select_estado = '';
                }
            }
            else {
                $scope.colonias = [];
                $scope.disabled_colonia = true;
                $scope.select_municipio = '';
                $scope.select_estado = '';
            }

        }
    }
);


