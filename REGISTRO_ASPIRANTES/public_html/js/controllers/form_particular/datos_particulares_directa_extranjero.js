
app.controller('DatosParticularesCtrl',
    function ($scope, $http, $window) {
        $scope.form = {
            id: 0,
            domicilio_extranjero: '',
            telefono_fijo: '',
            telefono_movil: '',
            correo_electronico: '',
            comfirm_correo_electronico: '',
            estado_civil: '',
            grupo_sanguineo: '',
            factor_rh: '',
            talla_bata: '',
            talla_uniforme_quirurgico: '',
            talla_pantalon: '',
            talla_zapatos: '',
            folio_enarm: '',
            cedula_profesional: '',
            ano_egreso_licenciatura: '',
            promedio_licenciatura: '',
            status: 1
        };
        $scope.button_disabled = false;
        get_datos_aspirante();

        function get_datos_aspirante() {
            $http({
                method: "GET",
                url: api_url + "datos_aspirantes/get_datos_aspirante_by_id"
            }).then(function (response) {
                $scope.form = response.data.datos_aspirante;
                $scope.form.comfirm_correo_electronico = response.data.datos_aspirante.correo_electronico;
            });
        }
        $scope.submit = function () {
            $scope.button_disabled = true;
            var data_send = angular.copy($scope.form);
            $http({
                method: "POST",
                url: api_url + 'datos_aspirantes/directa_extranjero_post',
                data: data_send
            }).then(function (response) {
                notify(response.data.mensaje, 'success');
                setTimeout(function () {
                    $window.location = base_url + 'form/form_archivos';
                }, 1000);
            }).catch(function (err) {
                if (err.status == 500) {
                    $scope.button_disabled = false;
                    notify(err.data.mensaje, 'error');
                } else if (err.status == 400) {
                    $scope.button_disabled = false;
                    notify(err.data.mensaje, 'error');
                } else if (err.status == 404) {
                    $scope.button_disabled = false;
                    notify(err.data.mensaje, 'error');
                }
            });
        }
    }
);


