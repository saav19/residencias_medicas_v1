app.controller('DatosParticularesCtrl',
    function ($scope, $http, $window) {
        $scope.form = {
            id: 0,
            domicilio_extranjero: '',
            correo_electronico: '',
            comfirm_correo_electronico: '',
            telefono_fijo: '',
            telefono_movil: '',
            estado_civil: '',
            grupo_sanguineo: '',
            factor_rh: '',
            talla_bata: '',
            talla_uniforme_quirurgico: '',
            talla_pantalon: '',
            talla_zapatos: '',
            folio_enarm: '',
            institucion_licenciatura: '',
            hospital_procedencia: '',
            cedula_profesional: '',
            ano_egreso_licenciatura: '',
            promedio_licenciatura: '',
            institucion_entidad_id: '',
            status: 1
        };
        $scope.universidades = [];
        $scope.entidades = [];
        $scope.colonias = [];
        $scope.disabled_colonia = true;
        select_municipio = '';
        select_estado = '';
        $scope.find_codigo_postal = '';
        $scope.nombre_convocatoria = '';
        $scope.curso = [];
        $scope.button_disabled = false;
        get_universidades();
        get_datos_aspirante();

        function get_universidades() {
            $http({
                method: "GET",
                url: api_url + "universidades/get_all_universidades"
            }).then(function (response) {
                $scope.universidades = response.data.universidades;
                get_entidades();
            });
        }
        function get_entidades() {
            $http({
                method: "GET",
                url: api_url + "entidades/get_all_entidades"
            }).then(function (response) {
                $scope.entidades = response.data.entidades;
            });
        }

        function get_datos_aspirante() {
            $http({
                method: "GET",
                url: api_url + "datos_aspirantes/get_datos_aspirante_by_id"
            }).then(function (response) {
                $scope.form = response.data.datos_aspirante;
                $scope.form.comfirm_correo_electronico = response.data.datos_aspirante.correo_electronico;
                $http({
                    method: "POST",
                    url: api_url + "codigos_postales/get_codigo_postal_by_codigo_postal_id",
                    data: { codigo_postal_id: $scope.form.codigo_postal_id }
                }).then(function (response) {
                    $scope.find_codigo_postal = response.data.codigo_postal.codigo_postal;
                });
            });
        }

        $scope.submit = function () {
            $scope.button_disabled = true;
            var data_send = angular.copy($scope.form);
            $http({
                method: "POST",
                url: api_url + 'datos_aspirantes/alta_especialidad_extranjero_post',
                data: data_send
            }).then(function (response) {
                notify(response.data.mensaje, 'success');
                setTimeout(function () {
                    $window.location = base_url + 'form/form_archivos';
                }, 1000);
            }).catch(function (err) {
                if (err.status == 500) {
                    $scope.button_disabled = false;
                    notify(err.data.mensaje, 'error');
                } else if (err.status == 400) {
                    $scope.button_disabled = false;
                    notify(err.data.mensaje, 'error');
                } else if (err.status == 404) {
                    $scope.button_disabled = false;
                    notify(err.data.mensaje, 'error');
                }
            });
        }

    }
);


