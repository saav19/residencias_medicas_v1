
app.controller('BienvenidaCtrl',
    function ($scope, $http, $window) {
        $scope.show_convocatorias_list = false;
        $scope.convocatorias = [];
        $scope.form = { convocatoria_id: '', curso_id: '', pais_id: '', tipo_convocatoria_id: '' };
        $scope.paises = [];
        $scope.cursos = [];
        $scope.year_uno = new Date().getFullYear() + 1;
        $scope.year_dos = new Date().getFullYear() + 2;
        get_convocatorias();
        get_catalogos();

        function get_convocatorias() {
            $http({
                method: "GET",
                url: api_url + "convocatorias/get_all_convocatorias"
            }).then(function (response) {
                $scope.convocatorias = response.data.convocatorias;
            }).catch(function (err) {

            });
        }
        function get_catalogos() {
            $http({
                method: "GET",
                url: api_url + "paises/get_all_paises"
            }).then(function (response) {
                $scope.paises = response.data.paises;
            });
        }

        $scope.submit = function () {
            var data_send = angular.copy($scope.form);
            $http({
                method: "POST",
                url: api_url + "registros/start_regsitro",
                data: data_send
            }).then(function () {
                $window.location = base_url + "form/form_general";
            }).catch(function (err) {
                if (err.status == 500) {
                    alert("error del servidor");
                } else if (err.status == 400) {
                    alert(err.data.error);
                }
            });
        }
        $scope.select_cursos = function () {
            var tipo_convocatoria_id = '';
            for (var index = 0; index < $scope.convocatorias.length; index++) {
                if ($scope.convocatorias[index].id == $scope.form.convocatoria_id) {
                    tipo_convocatoria_id = $scope.convocatorias[index].tipo_convocatoria_id;
                }
            }
            if (tipo_convocatoria_id != '') {
                $http({
                    method: "POST",
                    url: api_url + "cursos/get_cursos_by_tipo_convocatoria_id",
                    data: { tipo_convocatoria_id: tipo_convocatoria_id }
                }).then(function (response) {
                    $scope.cursos = response.data.cursos;
                    $scope.form.tipo_convocatoria_id = tipo_convocatoria_id;
                }).catch(function (err) {
                    if (err.status == 500) {
                        alert("error del servidor");
                    } else if (err.status == 400) {
                        alert(err.data.error);
                    }
                });
            }
        }
    }
);
