var app = angular.module('app', []);
app.controller('DatosArchivosCtrl',
    function ($scope, $http, $window) {
        var form = {};
        var curp = 'AAVS980519HMCLLB00';
        $scope.nombre_convocatoria = '';
        $scope.curso = [];
        get_convocatoria();
        function get_convocatoria() {
            $http({
                method: "GET",
                url: api_url + "convocatorias/get_convocatoria_by_id",
            }).then(function (response) {
                $scope.nombre_convocatoria = response.data.nombre_convocatoria;
                get_curso();
            });
        }
        function get_curso() {
            $http({
                method: "GET",
                url: api_url + "cursos/get_curso_by_id"
            }).then(function (response) {
                $scope.curso = response.data;
            });
        }
        $scope.submit = function () {
            get_files_inputs();
            if (validar_extencion_archivo()) {
                var data_send = new FormData();
                for (var index = 0 in form) {
                    if (form[index] != undefined) {
                        data_send.append(index, form[index]);
                    }
                }
                $http({
                    method: "POST",
                    url: api_url + "archivos_aspirantes/create_post",
                    data: data_send,
                    headers: { 'Content-Type': undefined }
                }).then(function (response) {
                    if (response.status == 200) {
                        //$window.location = base_url + 'form/form_particular';
                    }
                }).catch(function (err) {
                    if (err.status == 500) {
                        alert(err.data.mensaje);
                    } else if (err.status == 400) {
                        alert(err.data.error);
                    }
                });
            }
        }

        function get_files_inputs() {
            form = {
                file_constancia_enarm: document.getElementById('file_constancia_enarm').files[0],
                file_solicitud_ingreso: document.getElementById('file_solicitud_ingreso').files[0],
                file_acta_nacimiento: document.getElementById('file_acta_nacimiento').files[0],
                file_curp: document.getElementById('file_curp').files[0],
                file_rfc: document.getElementById('file_rfc').files[0],
                //file_ine: document.getElementById('file_ine').files[0],
                file_cartilla_militar: document.getElementById('file_cartilla_militar').files[0],
                file_comprobante_domicilio: document.getElementById('file_comprobante_domicilio').files[0],
                file_historial_academico: document.getElementById('file_historial_academico').files[0],
                file_terminacion_internado: document.getElementById('file_terminacion_internado').files[0],
                file_liberacion_servicio_social: document.getElementById('file_liberacion_servicio_social').files[0],
                file_titulo: document.getElementById('file_titulo').files[0],
                file_cedula_profesional: document.getElementById('file_cedula_profesional').files[0],
                file_carta_motivos: document.getElementById('file_carta_motivos').files[0],
                file_cartas_recomendacion: document.getElementById('file_cartas_recomendacion').files[0],
                file_certificado_medico: document.getElementById('file_certificado_medico').files[0],
                file_cartilla_vacunacion: document.getElementById('file_cartilla_vacunacion').files[0],
                file_cv: document.getElementById('file_cv').files[0],
                file_recibo_pago: document.getElementById('file_recibo_pago').files[0],
                file_carta_aceptacion: document.getElementById('file_carta_aceptacion').files[0],
                // file_pasaporte: document.getElementById('file_pasaporte').files[0],
                // file_constancia_solvencia_economica: document.getElementById('file_constancia_solvencia_economica').files[0],
            };
        }
        function validar_extencion_archivo() {
            for (var index = 0 in form) {
                if (form[index] != undefined) {
                    if (form[index].type != "application/pdf") {
                        alert("El archvio " + form[index].name + " no es pdf");
                        return false;
                    }
                }
            }
            return true;
        }

    }
);
