// angular 
app.controller('DatosArchivosCtrl',
    function ($scope, $http, $window) {
        $http({
            method: "GET",
            url: api_url + "archivos_aspirantes/create_carpeta",
        }).then(function (response) {

        }).catch(function (err) {

        });


        $scope.fin_registro = function () {
            $http({
                method: "GET",
                url: api_url + "registros/registro_finish",
            }).then(function (response) {
                $window.location = base_url + 'form/form_end';
            }).catch(function (err) {
                if (err.status == 500) {
                    $scope.button_disabled = false;
                    notify(err.data.mensaje, 'error');
                } else if (err.status == 400) {
                    $scope.button_disabled = false;
                    notify(err.data.mensaje, 'error');
                } else if (err.status == 404) {
                    $scope.button_disabled = false;
                    notify(err.data.mensaje, 'error');
                }
            });
        }
    }
);


//jquery

$(document).ready(function () {
    $('#view-pdf').attr('hidden', true);
    $("#select option[value='']").attr("selected", true);
    get_files_chek_user();

    $("#fileSubmit").submit(function (event) {
        select_item = $("#select").val();
        $.ajax({
            type: "POST",
            url: api_url + "archivos_aspirantes/uplod_file",
            data: new FormData(this),
            processData: false,
            contentType: false,
            statusCode: {
                200: function (response) {
                    notify(response.mensaje, 'success');
                    $('#documento_' + select_item).addClass("text-success");
                    get_file(select_item);
                },
                400: function (response) {
                    notify(response.responseJSON.mensaje, 'error');
                },
                403: function (response) {
                    notify(response.responseJSON.mensaje, 'error');
                },
                404: function (response) {
                    notify(response.responseJSON.mensaje, 'error');
                },
                500: function (response) {
                    notify(response.responseJSON.mensaje, 'error');
                }
            }
        });
        event.preventDefault();
    });

    $("#remove-file").click(function (event) {
        select_item = $("#select").val();
        $.ajax({
            type: "POST",
            url: api_url + "archivos_aspirantes/remove_file",
            data: { select: select_item },
            statusCode: {
                200: function (response) {
                    notify(response.mensaje, 'success');
                    $('#documento_' + select_item).removeClass("text-success");
                    get_file(select_item);
                   
                },
                400: function (response) {
                    notify(response.responseJSON.mensaje, 'error');
                },
                403: function (response) {
                    notify(response.responseJSON.mensaje, 'error');
                },
                404: function (response) {
                    notify(response.responseJSON.mensaje, 'error');
                },
                500: function (response) {
                    notify(response.responseJSON.mensaje, 'error');
                }
            }
        });
        event.preventDefault();
    });


    $("#select").change(function () {
        select_item = $("#select").val();
        get_file(select_item);
    });

    function get_file(select_item) {
        $.ajax({
            type: "POST",
            url: api_url + "archivos_aspirantes/get_file",
            data: { select: select_item },
            statusCode: {
                200: function (response) {
                    check_file(true, response.mensaje);
                },
                400: function (response) {
                    check_file(false, '');
                },
                403: function (response) {
                    check_file(false, '');
                },
                404: function (response) {
                    check_file(false, '');
                },
                500: function (response) {
                    check_file(false, '');
                }
            }
        });
    }

    function check_file(bool, ruta) {
        if (bool) {
            $('#view-pdf').attr('hidden', false);
            $('#sin_file').attr('hidden', true);
            $('#view-pdf').attr('data', ruta);
            $('#descarga').attr('href', ruta);
        } else {
            $('#view-pdf').attr('hidden', true);
            $('#sin_file').attr('hidden', false);
            $('#view-pdf').attr('data', ruta);
            $('#descarga').attr('href', ruta);
        }
    }

    function get_files_chek_user() {
        $.ajax({
            type: "GET",
            url: api_url + "archivos_aspirantes/get_files_exists",
            statusCode: {
                200: function (response) {
                    if (response.array.length != 0) {
                        for (let index = 0; index < response.array.length; index++) {
                            $('#documento_' + response.array[index]).addClass("text-success");
                        }
                    }
                },
                500: function (response) {
                    notify(response.responseJSON.mensaje, 'error');
                }
            }
        });
    }
  
});
