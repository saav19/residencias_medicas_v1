app.controller('DatosGeneralesCtrl',
    function ($scope, $http, $window) {
        $scope.form = { id: 0, nombre: '', apellido_1: '', apellido_2: '', genero: '', fecha_nacimiento: '', pasaporte: '', rfc: '', status: 1 };
        $scope.disabled = { nombre: false, apellido_1: false, apellido_2: false, genero: false, fecha_nacimiento: false, pasaporte: false, rfc: false };
        $scope.button_disabled = false;
        $scope.submit = function () {
            if ($scope.form.id == 0) {
                create_aspirante();
            } else {
                update_aspirante();
            }
        }

        function create_aspirante() {
            var data_send = angular.copy($scope.form);
            $http({
                method: "POST",
                url: api_url + "aspirantes/create_extranjero_post",
                data: data_send
            }).then(function (response) {
                if (response.status == 200) {
                    notify(response.data.mensaje, 'success');
                    setTimeout(function () {
                        $window.location = base_url + 'form/form_particular';
                    }, 1000);
                }
            }).catch(function (err) {
                if (err.status == 500) {
                    $scope.button_disabled = false;
                    notify(err.data.mensaje, 'error');
                } else if (err.status == 400) {
                    $scope.button_disabled = false;
                    notify(err.data.mensaje, 'error');
                } else if (err.status == 404) {
                    $scope.button_disabled = false;
                    notify(err.data.mensaje, 'error');
                }
            });
        }

        function update_aspirante() {
            var data_send = angular.copy($scope.form);
            var id = angular.copy($scope.form.id);
            $http({
                method: "POST",
                url: api_url + "aspirantes/update_extranjero_post",
                data: data_send,
                params: { id: id }
            }).then(function (response) {
                if (response.status == 200) {
                    notify(response.data.mensaje, 'success');
                    setTimeout(function () {
                        $window.location = base_url + 'form/form_particular';
                    }, 1000);
                }
            }).catch(function (err) {
                if (err.status == 500) {
                    $scope.button_disabled = false;
                    notify(err.data.mensaje, 'error');
                } else if (err.status == 400) {
                    $scope.button_disabled = false;
                    notify(err.data.mensaje, 'error');
                } else if (err.status == 404) {
                    $scope.button_disabled = false;
                    notify(err.data.mensaje, 'error');
                }
            });
        }

        $scope.find_aspirante_by_pasaporte = function () {
            var pasaporte_find = angular.copy($scope.form.pasaporte);
            if (pasaporte_find != null) {
                if (pasaporte_find.length >= 9 && pasaporte_find.length <= 15) {
                    $http({
                        method: "POST",
                        url: api_url + "aspirantes/get_aspirante_by_pasaporte",
                        data: { pasaporte: pasaporte_find }
                    }).then(function (response) {
                        if (response.status == 200) {
                            $scope.form = response.data.aspirante;
                            $scope.form.fecha_nacimiento = new Date($scope.form.fecha_nacimiento);
                            $scope.disabled = { nombre: true, apellido_1: true, apellido_2: true, genero: true, fecha_nacimiento: true };
                            if (response.data.aspirante.apellido_2 === null) {
                                $scope.disabled.apellido_2 = false;
                            }
                        }
                    });
                } else if (pasaporte_find.length < 9 ) {
                    $scope.form = { id: 0, nombre: '', apellido_1: '', apellido_2: '', genero: '', fecha_nacimiento: '', pasaporte: pasaporte_find, rfc: '', status: 1 };
                    $scope.disabled = { nombre: false, apellido_1: false, apellido_2: false, genero: false, fecha_nacimiento: false };
                }
            } else {
                $scope.form = { id: 0, nombre: '', apellido_1: '', apellido_2: '', genero: '', fecha_nacimiento: '', pasaporte: pasaporte_find, rfc: '', status: 1 };
                $scope.disabled = { nombre: false, apellido_1: false, apellido_2: false, genero: false, fecha_nacimiento: false };
            }
        }
    }

);