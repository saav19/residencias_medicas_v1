
app.controller('DatosGeneralesCtrl',
    function ($scope, $http, $window) {
        $scope.form = { id: 0, nombre: '', apellido_1: '', apellido_2: '', genero: '', fecha_nacimiento: '', curp: '', rfc: '', nacimiento_entidad_id: '', status: 1 };
        $scope.disabled = { nombre: false, apellido_1: false, apellido_2: false, genero: false, fecha_nacimiento: false, curp: false, rfc: false, entidad_id: false };
        $scope.entidades = [];
        $scope.button_disabled = false;
        get_entidades();

        function get_entidades() {
            $http({
                method: "GET",
                url: api_url + "entidades/get_all_entidades"
            }).then(function (response) {
                $scope.entidades = response.data.entidades;
            });
        }

        $scope.submit = function () {
            $scope.button_disabled = false;
            if ($scope.form.id == 0) {
                create_aspirante();
            } else {
                update_aspirante();
            }
        }

        function create_aspirante() {
            var data_send = angular.copy($scope.form);
            $http({
                method: "POST",
                url: api_url + "aspirantes/create_nacional_post",
                data: data_send
            }).then(function (response) {
                if (response.status == 200) {
                    notify(response.data.mensaje, 'success');
                    setTimeout(function () {
                        $window.location = base_url + 'form/form_particular';
                    }, 1000);
                }
            }).catch(function (err) {
                if (err.status == 500) {
                    $scope.button_disabled = false;
                    notify(err.data.mensaje, 'error');
                } else if (err.status == 400) {
                    $scope.button_disabled = false;
                    notify(err.data.mensaje, 'error');
                } else if (err.status == 404) {
                    $scope.button_disabled = false;
                    notify(err.data.mensaje, 'error');
                }
            });
        }


        function update_aspirante() {
            var data_send = angular.copy($scope.form);
            var id = angular.copy($scope.form.id);
            $http({
                method: "POST",
                url: api_url + "aspirantes/update_nacional_post",
                data: data_send,
                params: { id: id }
            }).then(function (response) {
                if (response.status == 200) {
                    notify(response.data.mensaje, 'success');
                    setTimeout(function () {
                        $window.location = base_url + 'form/form_particular';
                    }, 1000);
                }
            }).catch(function (err) {
                if (err.status == 500) {
                    $scope.button_disabled = false;
                    notify(err.data.mensaje, 'error');
                } else if (err.status == 400) {
                    $scope.button_disabled = false;
                    notify(err.data.mensaje, 'error');
                } else if (err.status == 404) {
                    $scope.button_disabled = false;
                    notify(err.data.mensaje, 'error');
                }
            });
        }

        $scope.find_aspirante_by_curp = function () {
            var curp_find = angular.copy($scope.form.curp);
            if (curp_find != null) {
                if (curp_find.length == 18) {
                    $http({
                        method: "POST",
                        url: api_url + "aspirantes/get_aspirante_by_curp",
                        data: { curp: curp_find }
                    }).then(function (response) {
                        if (response.status == 200) {
                            $scope.form = response.data.aspirante;
                            $scope.form.fecha_nacimiento = new Date($scope.form.fecha_nacimiento);
                            $scope.disabled = { nombre: true, apellido_1: true, apellido_2: true, genero: true, fecha_nacimiento: true, rfc: true, entidad_id: true };
                            if (response.data.aspirante.apellido_2 === null) {
                                $scope.disabled.apellido_2 = false;
                            }
                            if (response.data.aspirante.rfc === null) {
                                $scope.disabled.rfc = false;
                            }
                        }
                    });
                } else if (curp_find.length < 18) {
                    $scope.form = { id: 0, nombre: '', apellido_1: '', apellido_2: '', genero: '', fecha_nacimiento: '', curp: curp_find, rfc: '', entidad_id: '', status: 1 };
                    $scope.disabled = { nombre: false, apellido_1: false, apellido_2: false, genero: false, fecha_nacimiento: false, rfc: false, entidad_id: false };
                }
            } else {
                $scope.form = { id: 0, nombre: '', apellido_1: '', apellido_2: '', genero: '', fecha_nacimiento: '', curp: curp_find, rfc: '', entidad_id: '', status: 1 };
                $scope.disabled = { nombre: false, apellido_1: false, apellido_2: false, genero: false, fecha_nacimiento: false, rfc: false, entidad_id: false };
            }
        }
    }

);