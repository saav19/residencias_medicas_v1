app.controller('CabeceraFormCtrl',
    function ($scope, $http) {
        $scope.nombre_convocatoria = '';
        $scope.curso = [];
        get_convocatoria();

        function get_convocatoria() {
            $http({
                method: "GET",
                url: api_url + "convocatorias/get_convocatoria_by_id",
            }).then(function (response) {
                $scope.nombre_convocatoria = response.data.convocatoria.nombre_convocatoria;
                get_curso();
            });
        }

        function get_curso() {
            $http({
                method: "GET",
                url: api_url + "cursos/get_curso_by_id"
            }).then(function (response) {
                $scope.curso = response.data.curso;
            });
        }
    });