-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 23-05-2022 a las 19:14:20
-- Versión del servidor: 10.4.21-MariaDB
-- Versión de PHP: 8.0.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `convocatoria`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `entidades`
--

CREATE TABLE `entidades` (
  `id` int(10) UNSIGNED NOT NULL,
  `estado` varchar(50) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `entidades`
--

INSERT INTO `entidades` (`id`, `estado`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Aguascalientes', 1, '2022-05-12 13:38:19', NULL),
(2, 'Baja California', 1, '2022-05-12 13:38:19', NULL),
(3, 'Baja California Sur', 1, '2022-05-12 13:38:19', NULL),
(4, 'Campeche', 1, '2022-05-12 13:38:19', NULL),
(5, 'Coahuila de Zaragoza', 1, '2022-05-12 13:38:19', NULL),
(6, 'Colima', 1, '2022-05-12 13:38:19', NULL),
(7, 'Chiapas', 1, '2022-05-12 13:38:19', NULL),
(8, 'Chihuahua', 1, '2022-05-12 13:38:19', NULL),
(9, 'Ciudad de México', 1, '2022-05-12 13:38:19', NULL),
(10, 'Durango', 1, '2022-05-12 13:38:19', NULL),
(11, 'Guanajuato', 1, '2022-05-12 13:38:19', NULL),
(12, 'Guerrero', 1, '2022-05-12 13:38:19', NULL),
(13, 'Hidalgo', 1, '2022-05-12 13:38:19', NULL),
(14, 'Jalisco', 1, '2022-05-12 13:38:19', NULL),
(15, 'México', 1, '2022-05-12 13:38:19', NULL),
(16, 'Michoacán de Ocampo', 1, '2022-05-12 13:38:19', NULL),
(17, 'Morelos', 1, '2022-05-12 13:38:19', NULL),
(18, 'Nayarit', 1, '2022-05-12 13:38:19', NULL),
(19, 'Nuevo León', 1, '2022-05-12 13:38:19', NULL),
(20, 'Oaxaca', 1, '2022-05-12 13:38:19', NULL),
(21, 'Puebla', 1, '2022-05-12 13:38:19', NULL),
(22, 'Querétaro', 1, '2022-05-12 13:38:19', NULL),
(23, 'Quintana Roo', 1, '2022-05-12 13:38:19', NULL),
(24, 'San Luis Potosí', 1, '2022-05-12 13:38:19', NULL),
(25, 'Sinaloa', 1, '2022-05-12 13:38:19', NULL),
(26, 'Sonora', 1, '2022-05-12 13:38:19', NULL),
(27, 'Tabasco', 1, '2022-05-12 13:38:19', NULL),
(28, 'Tamaulipas', 1, '2022-05-12 13:38:19', NULL),
(29, 'Tlaxcala', 1, '2022-05-12 13:38:19', NULL),
(30, 'Veracruz de Ignacio de la Llave', 1, '2022-05-12 13:38:19', NULL),
(31, 'Yucatán', 1, '2022-05-12 13:38:19', NULL),
(32, 'Zacatecas', 1, '2022-05-12 13:38:19', NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `entidades`
--
ALTER TABLE `entidades`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `entidades`
--
ALTER TABLE `entidades`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
